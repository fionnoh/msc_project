//alpha_msbar_vs_mu_over_lambda.h
#ifndef _alpha_msbar_vs_mu_over_lambda    // To make sure you don't declare the function more 
#define _alpha_msbar_vs_mu_over_lambda    // than once by including the header multiple times.

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES

#include <gsl/gsl_integration.h>
#include <cmath>

const double pi{M_PI};
const double gamma_E{0.5772156649015328606065};

// forward definition of riemann zeta, found in riemann_zeta.cpp
double riemann_zeta(double);

// forward definition of param struct
struct my_f_params;
typedef struct {
    double value, error;
} my_tuple;

// Class Definition
// ================
class MyLambdaMSBarOverMuClass {

    private:
        my_f_params *p;

    public:
        double obj(double x, void * pars);
        double Getb0(double nf);
        double Getb1(double nf);
        double Getb2msbar(double nf);
        double Getb3msbar(double nf, double ZETA3);
        double Getb4msbar(double nf, double ZETA3, double ZETA4, double ZETA5);
        double Gett1plaq(double nf, double csw_plaq);
        double Gett2plaq(double nf, double csw_plaq);
        double Gett1lat(double nf, double csw);
        double Gett2lat(double nf, double csw);
        double Gett1qq(double nf);
        double Gett2qq(double nf, double ZETA3);
        my_tuple GetSolution_Pade_Approx( double b0, double b1, double b2_scheme, double g_squared, double g_squared_err );
        my_tuple GetSolution_Analytic_b2( double b0, double b1, double b2_scheme, double g_squared, double g_squared_err );
        my_tuple GetSolution_Analytic_b2_real( double b0, double b1, double b2_scheme, double g_squared, double g_squared_err );
        my_tuple GetSolution_Analytic_b2_complex( double b0, double b1, double b2_scheme, double g_squared, double g_squared_err );
        my_tuple GetSolution_Numerical( double b0, double b1, double g_squared, double g_squared_err );
        void setPars( my_f_params * xp ) { p = xp; };
        double getC( void )  ;
};

// param struct
    struct my_f_params {
        double b0;
        double b1;
        double b2;
        double b3;
        double b4;
        MyLambdaMSBarOverMuClass * pt_MyLambdaMSBarOverMuClass;
    };

    // Wrapper that points to member function
    // Trick: MyLambdaMSBarOverMuClass is an element of the my_f_params struct
    // so we can tease the value of the objective function out
    // of there.
    double gslClassWrapper(double x, void * pp) {
        my_f_params *p = (my_f_params *)pp;
        return p->pt_MyLambdaMSBarOverMuClass->obj(x,p);
    }

// Define Objective Function
// =========================

double MyLambdaMSBarOverMuClass::obj(double X, void * par) {

    // Integrand of the integral in F^s(g_s) from eqn 5 in hep-ph/0502212v2
    // and the stucture needed to input parameters
    my_f_params * params = (my_f_params *)p;
    double b0 = (params->b0);
    double b1 = (params->b1);
    double b2 = (params->b2);
    double b3 = (params->b3);
    double b4 = (params->b4);
    double f = -1.0/(b0*pow(X,3.0) + b1*pow(X,5.0) + b2*pow(X,7.0) + b3*pow(X,9.0) + b4*pow(X,11.0)) + 1.0/(b0*pow(X,3.0)) - b1/(pow(b0,2.0)*X);
    return f;
}

// Define beta function coefficients

double MyLambdaMSBarOverMuClass::Getb0(double nf) {
    return (1.0/pow(4.0*pi,2.0))*(11.0 - (2.0/3.0)*nf);
}
double MyLambdaMSBarOverMuClass::Getb1(double nf) {
    return (1.0/pow(4.0*pi,4.0))*(102.0 - (38.0/3.0)*nf);
}
double MyLambdaMSBarOverMuClass::Getb2msbar(double nf) {
    return (1.0/pow(4.0*pi,6.0))*(2857.0/2.0 - (5033.0/18.0)*nf + (325.0/54.0)*pow(nf,2.0));
}
double MyLambdaMSBarOverMuClass::Getb3msbar(double nf, double ZETA3) {
    return (1.0/pow(4.0*pi,8.0))*(149753.0/6.0 + 3564.0*ZETA3 - (1078361.0/162.0 + (6508.0/27.0)*ZETA3)*nf
                   + (50065.0/162.0 + (6472.0/81.0)*ZETA3)*pow(nf,2.0) + (1093.0/729.0)*pow(nf,3.0));
}
double MyLambdaMSBarOverMuClass::Getb4msbar(double nf, double ZETA3, double ZETA4, double ZETA5) {
    return (1.0/pow(4.0*pi,10.0))*(8157455.0/16.0 + (621885.0/2.0)*ZETA3 - (88209.0/2.0)*ZETA4 - 288090.0*ZETA5
                   + (-336460813.0/1944.0 - (4811164.0/81.0)*ZETA3 + (33935.0/6.0)*ZETA4 + (1358995.0/27.0)*ZETA5)*nf
                   + (25960913.0/1944.0 + (698531.0/81.0)*ZETA3 - (10526.0/9.0)*ZETA4 - (381760.0/81.0)*ZETA5)*pow(nf,2.0)
                   + (-630559.0/5832.0 - (48722.0/243.0)*ZETA3 + (1618.0/27.0)*ZETA4 + (460.0/9.0)*ZETA5)*pow(nf,3.0)
                   + (1205.0/2916.0 - (152.0/81.0)*ZETA3)*pow(nf,4.0));
}

// Define t_i plaquette/lattice coefficients

double MyLambdaMSBarOverMuClass::Gett1plaq(double nf, double csw_plaq) {
    return 0.1348680 - nf*(0.0066960 - 0.0050467*csw_plaq + 0.0298435*pow(csw_plaq, 2.0));
}
double MyLambdaMSBarOverMuClass::Gett2plaq(double nf, double csw_plaq) {
    return 0.0217565 - nf*(0.000753 - 0.001053*csw_plaq + 0.000498*pow(csw_plaq, 2.0)
                          - 0.000474*pow(csw_plaq, 3.0) - 0.000104*pow(csw_plaq, 4.0));
}
double MyLambdaMSBarOverMuClass::Gett1lat(double nf, double csw) {
    return 0.4682013 - nf*(0.0066960 - 0.0050467*csw + 0.0298435*pow(csw, 2.0));
}
double MyLambdaMSBarOverMuClass::Gett2lat(double nf, double csw) {
    return 0.0556675 - nf*(0.002600 + 0.000155*csw - 0.012834*pow(csw, 2.0)
                          - 0.000474*pow(csw, 3.0) - 0.000104*pow(csw, 4.0));
}
double MyLambdaMSBarOverMuClass::Gett1qq(double nf) {
    return -(1.0/pow(4*pi, 2.0))*(22.0*(gamma_E - 35.0/66.0) - (4.0/3.0)*nf*(gamma_E - 1.0/6.0));
}
double MyLambdaMSBarOverMuClass::Gett2qq(double nf, double ZETA3) {
    return (1.0/pow(4*pi, 4.0))*(1107.0/2.0 - 204.0*gamma_E - (229.0/3.0)*pow(pi, 2.0) + (9.0/4.0)*pow(pi, 4.0)
                                 - 66.0*ZETA3 + (nf/3.0)*(-553.0/3.0 + 76.0*gamma_E + (44.0/3.0)*pow(pi, 2.0) + 52.0*ZETA3)
                                 + (4.0/27.0)*pow(nf, 2.0)*(12.0 - pow(pi, 2.0)));
}

// Define Class members
// =====================
my_tuple MyLambdaMSBarOverMuClass::GetSolution_Analytic_b2_real( double b0, double b1, double b2_scheme, double g_squared, double g_squared_err) {
    // Analytic solution for b2 = 0
    double Amsbar, Bmsbar;
    double PAmsbar, PBmsbar;
    double b0_b1_const;
    double my_F, my_F_err;
    double my_A, my_B, my_C, my_D;
    double my_A_err, my_B_err, my_C_err, my_D_err;

    //Constants for analytic low order solution:
    Amsbar = b1 + sqrt(pow(b1,2.0) - 4.0*b0*b2_scheme);
    Bmsbar = b1 - sqrt(pow(b1,2.0) - 4.0*b0*b2_scheme);

    PAmsbar = -(b1)/(4.0*pow(b0,2.0)) - (pow(b1,2.0) 
              - 2.0*b0*b2_scheme)/(4.0*pow(b0,2.0)*sqrt(pow(b1,2.0) - 4.0*b0*b2_scheme));
    PBmsbar = -(b1)/(4.0*pow(b0,2.0)) + (pow(b1,2.0) 
              - 2.0*b0*b2_scheme)/(4.0*pow(b0,2.0)*sqrt(pow(b1,2.0) - 4.0*b0*b2_scheme));
    
    // useful vars that come up
    b0_b1_const = (0.5)*(b1)*(1.0/pow(b0,2.0));

    my_A = exp(-0.5*(1.0/g_squared)*(1.0/b0));
    my_A_err = 0.5*(1.0/b0)*my_A*g_squared_err/pow(g_squared,2.0);

    my_B = pow(b0*g_squared,-b0_b1_const);
    my_B_err = b0_b1_const*pow(b0*g_squared,-b0_b1_const - 1.0)*g_squared_err;

    my_C = pow(1.0 + 0.5*Amsbar*(1.0/b0)*g_squared, -PAmsbar);
    my_C_err = PAmsbar*pow(1.0 + 0.5*Amsbar*(1.0/b0)*g_squared, -PAmsbar-1.0)*g_squared_err;

    my_D = pow(1.0 + 0.5*Bmsbar*(1.0/b0)*g_squared, -PBmsbar);
    my_D_err = PBmsbar*pow(1.0 + 0.5*Bmsbar*(1.0/b0)*g_squared, -PBmsbar-1)*g_squared_err;

    my_F = my_A*my_B*my_C*my_D;
    my_F_err = my_F*sqrt(pow(my_A_err/my_A,2.0) + pow(my_B_err/my_B,2.0)
                             + pow(my_C_err/my_C,2.0) + pow(my_D_err/my_D,2.0));

    my_tuple return_my_tuple = {my_F, my_F_err};
    return return_my_tuple;
}


my_tuple MyLambdaMSBarOverMuClass::GetSolution_Analytic_b2_complex( double b0, double b1, double b2_scheme, double g_squared, double g_squared_err) {

    double my_xi, my_gamma, my_lambda, my_eta, my_kappa, my_chi;
    double b0_const, b0_b1_const;
    double my_F, my_F_err=0.0;

    // useful vars that come up
    b0_b1_const = (0.5)*(b1)*(1.0/pow(b0,2.0));

    //Constants for analytic low order solution:
    my_xi = 1.0 + 0.5*(b1/b0)*g_squared;
    my_gamma = sqrt(4.0*b0*b2_scheme - pow(b1,2.0));
    my_lambda = 0.5*g_squared*my_gamma/b0;
    my_eta = (1.0/16.0)*(1.0/pow(b0, 4.0))*(pow(b1, 2.0) - (pow(b1, 4.0)
              - 4.0*b0*pow(b1, 2.0)*b2_scheme + 4.0*pow(b0, 2.0)*pow(b2_scheme, 2.0))/pow(my_gamma, 2.0));
    my_kappa = (1.0/16.0)*(1.0/pow(b0, 4.0))*((2.0*pow(b1, 3.0) - 4.0*b0*b1*b2_scheme)/my_gamma);
    my_chi = exp(-2.0*atan(my_lambda/my_xi)*my_kappa);

    my_F = (exp(-0.5*(1.0/g_squared)*(1.0/b0))*pow(0.5*b0*g_squared,-b0_b1_const)*pow(pow(my_xi, 2.0) + pow(my_lambda, 2.0), my_eta)*my_chi);

    my_tuple return_my_tuple = {my_F, my_F_err};
    return return_my_tuple;
}


my_tuple MyLambdaMSBarOverMuClass::GetSolution_Analytic_b2( double b0, double b1, double b2_scheme, double g_squared, double g_squared_err) {

    double my_sqrt_arg = pow(b1, 2.0) - 4.0*b0*b2_scheme;
    if (my_sqrt_arg < 0)
    {
        return GetSolution_Analytic_b2_complex(b0, b1, b2_scheme, g_squared, g_squared_err);
    } else {
        return GetSolution_Analytic_b2_real(b0, b1, b2_scheme, g_squared, g_squared_err);
    }
}

my_tuple MyLambdaMSBarOverMuClass::GetSolution_Numerical( double b0, double b1, double g_squared, double g_squared_err) {
        
    // point the pt_MyLambdaMSBarOverMuClass element to this class
    p->pt_MyLambdaMSBarOverMuClass = this;

    // instantiate a gsl_function ...
    gsl_function F;
    F.params   = p;
    // ... and point the address of our
    // wrapper to it
    F.function = &gslClassWrapper;

    // set up GSL integration solver
    double result, error;

    // useful vars that come up
    double b0_b1_const;
    b0_b1_const = (0.5)*(b1)*(1.0/pow(b0,2.0));

    gsl_integration_workspace*w
      = gsl_integration_workspace_alloc (1000);

    double upper_limit = sqrt(g_squared);

    gsl_integration_qags (&F, 0, upper_limit, 0, 1e-6, 1000,
                          w, &result, &error);

    gsl_integration_workspace_free (w);

    double my_A, my_B, my_C;
    double my_A_err, my_B_err, my_C_err;
    double my_F, my_F_err;

    my_A = exp(-0.5*(1.0/g_squared)*(1.0/b0));
    my_A_err = 0.5*(1.0/b0)*my_A*g_squared_err/pow(g_squared,2.0);

    my_B = pow(b0*g_squared,-b0_b1_const);
    my_B_err = b0_b1_const*pow(b0*g_squared,-b0_b1_const - 1.0)*g_squared_err;

    my_C = exp(-result);
    my_C_err = my_C*error;

    my_F = my_A*my_B*my_C;
    my_F_err = my_F*sqrt(pow(my_A_err/my_A,2.0) + pow(my_B_err/my_B,2.0)
                             + pow(my_C_err/my_C,2.0));

    my_tuple return_my_tuple = {my_F, my_F_err};
    return return_my_tuple;
}

my_tuple MyLambdaMSBarOverMuClass::GetSolution_Pade_Approx( double b0, double b1, double b2_scheme, double g_squared, double g_squared_err) {
    // Padé approximation
    
    double b0_b1_const_1, b0_b1_const_2, b2_b1_const;
    double my_A, my_B, my_C;
    double my_A_err, my_B_err, my_C_err;
    double my_F, my_F_err;
    
    // useful vars that come up
    b0_b1_const_1 = b1/b0;
    b0_b1_const_2 = (0.5)*(b1)*(1.0/pow(b0,2.0));
    b2_b1_const = b2_scheme/b1;

    my_A = exp(-0.5*(1.0/g_squared)*(1.0/b0));
    my_A_err = 0.5*(1.0/b0)*my_A*g_squared_err/pow(g_squared,2.0);

    my_B = pow(b0*g_squared, -b0_b1_const_2);
    my_B_err = b0*b0_b1_const_2*pow(b0*g_squared, -b0_b1_const_2-1.0)*g_squared_err;

    my_C = pow(1.0/(1.0 + (b0_b1_const_1 - b2_b1_const)*g_squared), -b0_b1_const_2);
    my_C_err = (b0_b1_const_1 - b2_b1_const)*b0_b1_const_2
               *pow(1.0/(1.0 + (b0_b1_const_1 - b2_b1_const)*g_squared), -b0_b1_const_2 + 1.0)
               *g_squared_err;

    my_F = my_A*my_B*my_C;
    my_F_err = my_F*sqrt(pow(my_A_err/my_A,2.0) + pow(my_B_err/my_B,2.0)
                             + pow(my_C_err/my_C,2.0));

    my_tuple return_my_tuple = {my_F, my_F_err};
    return return_my_tuple;
}

#endif
#endif