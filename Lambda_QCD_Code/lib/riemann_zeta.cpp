/*
A function to find the Rieman Zeta of a parameter s
*/

#include <cmath>
#include <iostream>
using namespace std;

double riemann_zeta(double s)
{
    // declare vars
    const double precision = 1e-19;
    double result = 0.0;
    double addition = 1.0;

    for (int i = 2; abs(addition) > precision; ++i){
        result += addition;
        addition = 1.0/pow(i,s);
    }

    return result;
}