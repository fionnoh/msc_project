/*
Creating the arrays to be used to plot alpa_strong msbar
versus mu over Lambda msbar
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <jsoncpp/dist/json/json.h>
#include <cmath>
#include <fstream>
#include <gsl/gsl_integration.h>
#include "../../headers/Lambda_over_mu.h"
using namespace std;

typedef struct {
    double value, error, mu_star;
} my_truple;

my_truple my_r0Lmsbar_function(MyLambdaMSBarOverMuClass C, double b0, double b1, double csw,
                            double nf, double beta, double scale, double scale_err,
                            double P, double P_err, double my_c)
{
    // Finding values for ti_plaq
    double csw_plaq = csw*pow(P, 0.75); 
    double t1_plaq, t2_plaq;

    t1_plaq = C.Gett1plaq(nf, csw_plaq);
    t2_plaq = C.Gett2plaq(nf, csw_plaq);
    double my_mu_star = exp(0.5*t1_plaq/b0 + my_c);

    double my_gsquared0 = 6.0/beta;
    double my_gsquared_plaq = my_gsquared0/P;
    double my_gsquared_plaq_err = my_gsquared0*P_err/pow(P,2.0);

    double my_gmsbarsquared = 1.0/(1.0/my_gsquared_plaq + 2*b0*my_c
                              + (b1*t1_plaq/b0 + 2*b1*my_c - t2_plaq)*my_gsquared_plaq);
    double my_gmsbarsquared_err = pow(my_gmsbarsquared,2.0)
                                *sqrt( pow(my_gsquared_plaq_err/pow(my_gsquared_plaq,2.0),2.0) 
                                  + pow( (b1*t1_plaq/b0 - t2_plaq)*my_gsquared_plaq_err,2.0));

    my_tuple my_F_my_tuple;
    double my_F, my_F_err;

    my_F_my_tuple = C.GetSolution_Numerical(b0, b1, my_gmsbarsquared, my_gmsbarsquared_err);
    my_F = my_F_my_tuple.value;
    my_F_err = my_F_my_tuple.error;

    double r0_Lambda_msbar_json = scale*my_mu_star*my_F;

    double r0_Lambda_msbar_json_err = r0_Lambda_msbar_json
                                      *sqrt(pow(scale_err/scale,2.0) 
                                      + pow(my_F_err/my_F,2.0));

    my_truple return_my_truple = {r0_Lambda_msbar_json, r0_Lambda_msbar_json_err, my_mu_star};
    return return_my_truple;
}


int main(){
    MyLambdaMSBarOverMuClass C;

    // Input values
    double nf = 2.0;

    string string_scale = "r0_new";

    bool b4_bool = 1;

    // strings used for filenames
    string string_quenched = "";
    string string_b4 = "";
    if (nf != 0.0) {
        string_quenched = "un";
    }

    string string_table = string_scale + "_over_a_" + string_quenched + "quenched";

    std::cout << string_table << std::endl;

    double b0, b1, b2msbar, b3msbar, b4msbar;

    double ZETA3 = riemann_zeta(3.0);
    double ZETA4 = riemann_zeta(4.0);
    double ZETA5 = riemann_zeta(5.0);

    // vars as defined in paper
    b0 = C.Getb0(nf);
    b1 = C.Getb1(nf);
    b2msbar = C.Getb2msbar(nf);
    b3msbar = C.Getb3msbar(nf, ZETA3);
    b4msbar = 0.0;
    if (b4_bool != 0) {
        b4msbar = C.Getb4msbar(nf, ZETA3, ZETA4, ZETA5);
        string_b4 = "_b4";
    }
    std::cout << "b4 = " << b4msbar << std::endl;
    
    
    // allocate the param structure
    my_f_params myp;
    myp.b0 = b0;
    myp.b1 = b1;
    myp.b2 = b2msbar;
    myp.b3 = b3msbar;
    myp.b4 = b4msbar;

    // point to myp from inside the class
    C.setPars( &myp );

    // Getting values from table 3 in in hep-ph/0502212v2
    Json::Value table;
    std::ifstream table_file("./data/" + string_table + ".json");
    table_file >> table;
    table_file.close();

    int _array_size = table["P"].size();

    double csw[_array_size];

    for (int i=0; i<_array_size; ++i){
        if (nf != 0.0)
        {
            csw[i] = table["csw"][i].asDouble();
        } else {
            csw[i] = 0.0;
        }
    }

    int array_size = 601;
    // json for output
    Json::Value output_json, r0_Lambda_msbar_json, r0_Lambda_msbar_err_json, my_mu_star_json;

    int j;
    std::cout << "Please enter a value for j." << std::endl;
    std::cin >> j;

    std::cout << table["beta"][j] << std::endl;
    for (int i=0; i<array_size; ++i){
        my_truple r0_Lambda_msbar_truple = my_r0Lmsbar_function(C, b0, b1, csw[j], nf, table["beta"][j].asDouble(),
                                                       table["scale"][j].asDouble(),
                                                       table["scale_err"][j].asDouble(),
                                                       table["P"][j].asDouble(),
                                                       table["P_err"][j].asDouble(),
                                                       -3.0 + i*0.01);

        r0_Lambda_msbar_json[i] = r0_Lambda_msbar_truple.value;
        r0_Lambda_msbar_err_json[i] = r0_Lambda_msbar_truple.error;
        my_mu_star_json[i] = r0_Lambda_msbar_truple.mu_star;
    }
    output_json["r0Lmsbar"] = r0_Lambda_msbar_json;
    output_json["mu_star"] = my_mu_star_json;

    Json::StyledWriter writer;
    const string output = writer.write(output_json);

    string filename = "./data/" + string_scale + "_Lambda_msbar_" + string_quenched
                      + "quenched_MI_mu_star" + string_b4 + ".json";

    ofstream my_file;
    my_file.open (filename);
    my_file << output;
    my_file.close();
    
    // std::cout << output << std::endl;

    return 0;
}