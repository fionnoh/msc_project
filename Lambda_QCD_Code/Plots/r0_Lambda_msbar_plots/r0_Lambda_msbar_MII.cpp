/*
Creating the arrays to be used to plot alpa_strong msbar
versus mu over Lambda msbar
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <jsoncpp/dist/json/json.h>
#include <cmath>
#include <fstream>
#include <gsl/gsl_integration.h>
#include "../../headers/Lambda_over_mu.h"
using namespace std;


my_tuple my_r0Lmsbar_function(MyLambdaMSBarOverMuClass C, double b0, double b1,
                              double b2msbar, bool pade_bool, bool lat_bool, double csw, double nf,
                              double beta, double scale, double scale_err,
                              double P, double P_err)
{
    // Finding values for ti and b2
    double t1, t2, b2;
    double csw_plaq;
    double my_gsquared0, my_gsquared_plaq, my_gsquared_plaq_err;
    // Choice between using the g_0 (lattice) or g_plaq
    // expansion of 1/g_msbar^2
    if (lat_bool != 0) {
        csw_plaq = csw;
        t1 = C.Gett1lat(nf, csw_plaq);
        t2 = C.Gett2lat(nf, csw_plaq);
        b2 = b2msbar + b1*t1 - b0*t2;

        my_gsquared0 = 6.0/beta;
        // Poor nomclemanture:
        my_gsquared_plaq = my_gsquared0;
        // Lazy errors:
        my_gsquared_plaq_err = 0;
    } else {
        csw_plaq = csw*pow(P, 0.75);
        t1 = C.Gett1plaq(nf, csw_plaq);
        t2 = C.Gett2plaq(nf, csw_plaq);
        b2 = b2msbar + b1*t1 - b0*t2;

        my_gsquared0 = 6.0/beta;
        my_gsquared_plaq = my_gsquared0/P;
        my_gsquared_plaq_err = my_gsquared0*P_err/pow(P,2.0);
    }
    my_tuple my_F_my_tuple;
    double my_F, my_F_err;

    if (pade_bool != 0) {
        my_F_my_tuple = C.GetSolution_Pade_Approx(b0, b1, b2,
                                                   my_gsquared_plaq, my_gsquared_plaq_err);
    } else {
        my_F_my_tuple = C.GetSolution_Analytic_b2(b0, b1, b2,
                                                   my_gsquared_plaq, my_gsquared_plaq_err);
    }
    my_F = my_F_my_tuple.value;
    my_F_err = my_F_my_tuple.error;
    
    double my_r0_Lambda_plaq = scale*my_F;
    double my_r0_Lambda_plaq_err = my_r0_Lambda_plaq*sqrt(pow(scale_err/scale, 2.0)
                                                          + pow(my_F_err/my_F, 2.0));
    double r0_Lambda_msbar_json = my_r0_Lambda_plaq*exp(0.5*t1/b0);
    double r0_Lambda_msbar_json_err = my_r0_Lambda_plaq_err*exp(0.5*t1/b0);

    my_tuple return_my_tuple = {r0_Lambda_msbar_json, r0_Lambda_msbar_json_err};
    return return_my_tuple;
}

int main(){
    MyLambdaMSBarOverMuClass C;

    double nf;
    std::cout << "Please enter a value for nf." << std::endl;
    std::cin >> nf;
    std::cout << "nf = " << nf << std::endl;

    string string_scale;
    std::cout << "Please enter a which scale you would like to use (r0, w0i, t0i)." << std::endl;
    std::cin >> string_scale;
    std::cout << "string_scale = " << string_scale << std::endl;

    bool pade_bool;
    std::cout << "Do you want to compute with the Padé approximation? (0:N, 1:Y)" << std::endl;
    std::cin >> pade_bool;

    bool lat_bool;
    std::cout << "Do you want to compute with the lattice expansion? (0:N, 1:Y)" << std::endl;
    std::cin >> lat_bool;

    // strings used for filenames
    string string_quenched = "";
    string string_pade;
    string string_lat;
    if (nf != 0.0) {
        string_quenched = "un";
    }

    string string_table = string_scale + "_over_a_" + string_quenched + "quenched";

    if (pade_bool != 0) {
        string_pade = "P";
    }

    if (lat_bool != 0) {
        string_lat = "_lat";
    }

    double b0, b1, b2msbar;

    // vars as defined in paper
    b0 = C.Getb0(nf);
    b1 = C.Getb1(nf);
    b2msbar = C.Getb2msbar(nf);

    // Getting values from table 3 in in hep-ph/0502212v2
    Json::Value table;
    std::ifstream table_file("./data/" + string_table + ".json");
    table_file >> table;
    table_file.close();

    int array_size = table["P"].size();

    double csw[array_size];
    if (nf != 0.0) {
        for (int i=0; i<array_size; ++i){
            csw[i] = table["csw"][i].asDouble();
        }
    } else {
        for (int i=0; i<array_size; ++i){
            csw[i] = 0.0;
        }
    }

    // json for output
    Json::Value output_json, r0_Lambda_msbar_json, r0_Lambda_msbar_err_json;

    for (int i=0; i<array_size; ++i){
        my_tuple r0_Lambda_msbar_tuple = my_r0Lmsbar_function(C, b0, b1, b2msbar, pade_bool, lat_bool,
                                                              csw[i], nf,
                                                              table["beta"][i].asDouble(),
                                                              table["scale"][i].asDouble(),
                                                              table["scale_err"][i].asDouble(),
                                                              table["P"][i].asDouble(),
                                                              table["P_err"][i].asDouble());

        r0_Lambda_msbar_json[i] = r0_Lambda_msbar_tuple.value;
        r0_Lambda_msbar_err_json[i] = r0_Lambda_msbar_tuple.error;
    }

    output_json["r0Lmsbar"] = r0_Lambda_msbar_json;
    output_json["r0Lmsbar_err"] = r0_Lambda_msbar_err_json;
    output_json["scale"] = table["scale"];
    output_json["scale_err"] = table["scale_err"];

    Json::StyledWriter writer;
    const string output = writer.write(output_json);

    string filename = "./data/" + string_scale + "_Lambda_msbar_" + string_quenched + "quenched_MII"
                       + string_pade + string_lat + ".json";

    std::cout << filename << std::endl;

    ofstream my_file;
    my_file.open (filename);
    my_file << output;
    my_file.close();

    std::cout << output << std::endl;

    return 0;
}