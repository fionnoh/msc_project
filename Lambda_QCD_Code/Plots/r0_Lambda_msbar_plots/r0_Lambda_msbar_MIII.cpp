/*
Creating the arrays to be used to plot alpa_strong msbar
versus mu over Lambda msbar
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <jsoncpp/dist/json/json.h>
#include <cmath>
#include <fstream>
#include <gsl/gsl_integration.h>
#include "../../headers/Lambda_over_mu.h"
using namespace std;

my_tuple my_r0Lmsbar_function(MyLambdaMSBarOverMuClass C, double b0, double b1,
                              double b2_plaq, bool pade_bool,
                              double nf, double beta,
                              double scale, double scale_err, double P, double P_err)
{
    double my_gsquared0 = 6.0/beta;
    double my_gsquared_plaq = my_gsquared0/P;
    double csw_plaq = 1.0 + 0.0159*my_gsquared_plaq;
    double my_gsquared_plaq_err = my_gsquared0*P_err/pow(P,2.0);
    double t1_plaq = C.Gett1plaq(nf, 1.0);

    my_tuple my_F_my_tuple;
    double my_F, my_F_err;
    if (pade_bool != 0) {
        my_F_my_tuple = C.GetSolution_Pade_Approx(b0, b1, b2_plaq, my_gsquared_plaq, my_gsquared_plaq_err);
    } else {
        my_F_my_tuple = C.GetSolution_Analytic_b2(b0, b1, b2_plaq, my_gsquared_plaq, my_gsquared_plaq_err);
    }
    my_F = my_F_my_tuple.value;
    my_F_err = my_F_my_tuple.error;

    double my_r0_Lambda_plaq = scale*my_F;
    double my_r0_Lambda_plaq_err = my_r0_Lambda_plaq*sqrt(pow(scale_err/scale, 2.0)
                                                          + pow(my_F_err/my_F, 2.0));

    double r0_Lambda_msbar_json = my_r0_Lambda_plaq*exp(0.5*t1_plaq/b0);
    double r0_Lambda_msbar_json_err = my_r0_Lambda_plaq_err*exp(0.5*t1_plaq/b0);

    my_tuple return_my_tuple = {r0_Lambda_msbar_json, r0_Lambda_msbar_json_err};
    return return_my_tuple;
}

int main(){
    MyLambdaMSBarOverMuClass C;

    // As defined in paper
    double nf = 2.0;
    std::cout << "nf = " << nf << std::endl;

    string string_scale;
    std::cout << "Please enter a which scale you would like to use (r0)." << std::endl;
    std::cin >> string_scale;
    std::cout << "string_scale = " << string_scale << std::endl;

    bool pade_bool;
    std::cout << "Do you want to compute with the Padé approximation? (0:N, 1:Y)" << std::endl;
    std::cin >> pade_bool;

    string string_table = string_scale + "_over_a_unquenched";

    // strings used for filenames
    string string_pade = "";
    if (pade_bool != 0) {
        string_pade = "P";
    }

    double b0, b1;
    double b2_plaq = -0.0008241;

    // vars as defined in paper
    b0 = C.Getb0(nf);
    b1 = C.Getb1(nf);

    // Getting values from table 3 in in hep-ph/0502212v2
    Json::Value table;
    std::ifstream table_file("./data/" + string_table + ".json");
    table_file >> table;
    table_file.close();

    int array_size = table["P"].size();

    // json for output

    Json::Value output_json, r0_Lambda_msbar_json, r0_Lambda_msbar_err_json;


    for (int i=0; i<array_size; ++i){
        my_tuple r0_Lambda_msbar_tuple = my_r0Lmsbar_function(C, b0, b1, b2_plaq, pade_bool, nf,
                                                       table["beta"][i].asDouble(),
                                                       table["scale"][i].asDouble(),
                                                       table["scale_err"][i].asDouble(),
                                                       table["P"][i].asDouble(),
                                                       table["P_err"][i].asDouble());

        r0_Lambda_msbar_json[i] = r0_Lambda_msbar_tuple.value;
        r0_Lambda_msbar_err_json[i] = r0_Lambda_msbar_tuple.error;
    }
    
    output_json["r0Lmsbar"] = r0_Lambda_msbar_json;
    output_json["r0Lmsbar_err"] = r0_Lambda_msbar_err_json;
    output_json["scale"] = table["scale"];
    output_json["scale_err"] = table["scale_err"];

    Json::StyledWriter writer;
    const string output = writer.write(output_json);

    string filename = "./data/" + string_scale + "_Lambda_msbar_unquenched_MIII" + string_pade +".json";

    ofstream my_file;
    my_file.open (filename);
    my_file << output;
    my_file.close();
    
    std::cout << output << std::endl;

    return 0;
}