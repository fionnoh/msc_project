/*
Creating the arrays to be used to plot alpa_strong msbar
versus mu over Lambda msbar
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <jsoncpp/dist/json/json.h>
#include <cmath>
#include <fstream>
#include <gsl/gsl_integration.h>
#include "../../headers/Lambda_over_mu.h"
using namespace std;

double my_gplaq_squared_function(MyLambdaMSBarOverMuClass C, double b0, double b1, double csw,
                            double nf, double beta, double P, double scale, double mu)
{
    // Finding values for ti_plaq
    double csw_plaq = csw*pow(P, 0.75); 
    double t1_plaq, t2_plaq;

    t1_plaq = C.Gett1plaq(nf, csw_plaq);
    t2_plaq = C.Gett2plaq(nf, csw_plaq);

    double my_gsquared0 = 6.0/beta;
    double my_gsquared_plaq = my_gsquared0/P;

    double my_gmsbarsquared = 1.0/1.0/(1.0/my_gsquared_plaq + 2*b0*log(scale*mu) - t1_plaq
                              + (2*b1*log(scale*mu) - t2_plaq)*my_gsquared_plaq);
    return my_gmsbarsquared;
}

double my_glat_squared_function(MyLambdaMSBarOverMuClass C, double b0, double b1, double csw,
                            double nf, double beta, double scale, double P, double mu)
{
    // Finding values for ti_lat 
    double t1_lat, t2_lat;

    t1_lat = C.Gett1lat(nf, csw);
    t2_lat = C.Gett2lat(nf, csw);

    double my_gsquared0 = 6.0/beta;
    double my_gmsbarsquared = 1.0/(1.0/my_gsquared0 + 2*b0*log(scale*mu) - t1_lat
                              + (2*b1*log(scale*mu) - t2_lat)*my_gsquared0);
    return my_gmsbarsquared;
}

int main(){
    MyLambdaMSBarOverMuClass C;

    // Input values
    double nf = 0.0;

    string string_scale = "r0";

    // strings used for filenames
    string string_quenched = "";
    if (nf != 0.0) {
        string_quenched = "un";
    }

    string string_table = string_scale + "_over_a_" + string_quenched + "quenched";

    double b0, b1, b2msbar, b3msbar, b4msbar;

    // vars as defined in paper
    b0 = C.Getb0(nf);
    b1 = C.Getb1(nf);
    b2msbar = 0.0;
    b3msbar = 0.0;
    b4msbar = 0.0;    
    
    // allocate the param structure
    my_f_params myp;
    myp.b0 = b0;
    myp.b1 = b1;
    myp.b2 = b2msbar;
    myp.b3 = b3msbar;
    myp.b4 = b4msbar;

    // point to myp from inside the class
    C.setPars( &myp );

    // Getting values from table 3 in in hep-ph/0502212v2
    Json::Value table;
    std::ifstream table_file("../r0_Lambda_msbar_plots/data/" + string_table + ".json");
    table_file >> table;
    table_file.close();

    int _array_size = table["P"].size();

    double csw[_array_size];

    for (int i=0; i<_array_size; ++i){
        if (nf != 0.0)
        {
            csw[i] = table["csw"][i].asDouble();
        } else {
            csw[i] = 0.0;
        }
    }

    int array_size = 1001;
    double my_mu[array_size];
    // json for output
    Json::Value output_json, g_msbar_squared_json, my_mu_json;

    for (int j=0; j<_array_size; ++j){
    
        for (int i=0; i<array_size; ++i){
            my_mu[i] = 0.03 + i*0.01;
            double my_returned = my_gplaq_squared_function(C, b0, b1, csw[j], nf, table["beta"][j].asDouble(),
                                                           table["scale"][j].asDouble(),
                                                           table["P"][j].asDouble(),
                                                           my_mu[i]);
            g_msbar_squared_json[i] = my_returned;
            my_mu_json[i] = my_mu[i];

        }
        double double_beta = round(table["beta"][j].asDouble()*100)/100;
        string string_beta = to_string(double_beta);
        output_json["g_msbar_squared"][string_beta] = g_msbar_squared_json;
    }
    output_json["mu"] = my_mu_json;
    Json::StyledWriter writer;
    const string output = writer.write(output_json);

    string filename = "./data/" + string_scale + "_g_squared_plaq"
                      + ".json";

    ofstream my_file;
    my_file.open (filename);
    my_file << output;
    my_file.close();
    

    return 0;
}