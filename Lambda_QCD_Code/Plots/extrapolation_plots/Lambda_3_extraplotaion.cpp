/*
Creating the arrays to be used to plot alpa_strong msbar
versus mu over Lambda msbar
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <jsoncpp/dist/json/json.h>
#include <cmath>
#include <fstream>
#include <gsl/gsl_integration.h>
#include "../../headers/Lambda_over_mu.h"
using namespace std;


my_tuple my_rLmsbar_function(MyLambdaMSBarOverMuClass C, double b0, double b1,
                              double b2_qq, double t1_qq, double nf,
                              double my_gsquared)
{
    my_tuple my_F_my_tuple;
    double my_F, my_F_err;

    my_F_my_tuple = C.GetSolution_Numerical(b0, b1, my_gsquared, 0.01 );

    my_F = my_F_my_tuple.value;
    my_F_err = my_F_my_tuple.error;
    
    double r_Lambda_msbar_json = my_F*exp(0.5*t1_qq/b0);
    double r_Lambda_msbar_json_err = my_F_err*exp(0.5*t1_qq/b0);

    my_tuple return_my_tuple = {r_Lambda_msbar_json, r_Lambda_msbar_json_err};
    return return_my_tuple;
}

int main(){
    MyLambdaMSBarOverMuClass C;

    // allocate the param structure
    my_f_params myp;
for (double nf=0.0; nf<4.0; ++nf){

    std::cout << nf << std::endl;

    // strings used for filenames
    string string_nf = "nf0";
    if (nf == 1.0) {
        string_nf = "nf1";
    } else if (nf == 2.0){
        string_nf = "nf2";
    } else if (nf == 3.0){
        string_nf = "nf3";
    }

    double b0, b1, b2msbar;

    // vars as defined in paper
    b0 = C.Getb0(nf);
    b1 = C.Getb1(nf);
    b2msbar = C.Getb2msbar(nf);


    // Finding values for ti_qq and b2_qq
    double t1_qq, t2_qq, b2_qq;
    double ZETA3 = riemann_zeta(3.0);

    t1_qq = C.Gett1qq(nf);
    t2_qq = C.Gett2qq(nf, ZETA3);
    b2_qq = b2msbar + b1*t1_qq - b0*t2_qq;

    myp.b0 = b0;
    myp.b1 = b1;
    myp.b2 = b2_qq;
    myp.b3 = 0.0;
    myp.b4 = 0.0;

    // point to myp from inside the class
    C.setPars( &myp );

    int array_size = 690;

    // json for output
    Json::Value output_json, r_Lambda_msbar_json;

    for (int i=0; i<array_size; ++i){
        my_tuple r_Lambda_msbar_tuple = my_rLmsbar_function(C, b0, b1,
                                                              b2_qq, t1_qq, nf,
                                                              (i+1)*0.5);

        r_Lambda_msbar_json[i] = r_Lambda_msbar_tuple.value;
    }

    output_json["rLmsbar"] = r_Lambda_msbar_json;

    Json::StyledWriter writer;
    const string output = writer.write(output_json);

    string filename = "./data/r_Lambda_msbar_" + string_nf + "_three_loops.json";

    ofstream my_file;
    my_file.open (filename);
    my_file << output;
    my_file.close();

    // std::cout << output << std::endl;
}
    return 0;
}