/*
Creating the arrays to be used to plot alpa_strong msbar
versus mu over Lambda msbar
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <jsoncpp/dist/json/json.h>
#include <cmath>
#include <fstream>
#include <gsl/gsl_integration.h>
#include "../../headers/Lambda_over_mu.h"
using namespace std;


int main(){
    MyLambdaMSBarOverMuClass C;

    // declare vars
    double ZETA3 = riemann_zeta(3.0);
    double ZETA4 = riemann_zeta(4.0);
    double ZETA5 = riemann_zeta(5.0);
    // As defined in paper
    double nf = 0.0;
    std::cout << "nf = " << nf << std::endl;
    double b0, b1, b2msbar, b3msbar, b4msbar;

    // x = mu/Lambda
    double x0, x1, x2, x3, x4, xpade;
    double alpha = 1.0/(2.0*pi);

    // strings used for filenames
    string string_const = "";
    if (nf != 0.0)
    {
        string_const = "_nf_eq_2";
    }

    // vars as defined in paper
    b0 = C.Getb0(nf);
    b1 = C.Getb1(nf);
    b2msbar = C.Getb2msbar(nf);
    b3msbar = C.Getb3msbar(nf, ZETA3);
    b4msbar = C.Getb4msbar(nf, ZETA3, ZETA4, ZETA5);
    
    // allocate the param structure
    my_f_params myp;
    myp.b0=b0;
    myp.b1=b1;
    myp.b2=b2msbar;
    myp.b3=b3msbar;
    myp.b4=0.0;

    // point to myp from inside the class
    C.setPars( &myp );
    
    x0 = C.GetSolution_Analytic_b1(b0, 0.0, 0.0, alpha);
    x1 = C.GetSolution_Analytic_b1(b0, b1, 0.0, alpha);
    x2 = C.GetSolution_Analytic_b2(b0, b1, b2msbar, alpha);
    x3 = C.GetSolution_Numerical(b0, b1, alpha);
    myp.b4=b4msbar;
    x4 = C.GetSolution_Numerical(b0, b1, alpha);
    xpade = C.GetSolution_Pade_Approx(b0, b1, b2msbar, alpha);

    Json::Value FMSbar_json;
    FMSbar_json["x0"] = x0;
    FMSbar_json["x1"] = x1;

    FMSbar_json["x2"] = x2;
    FMSbar_json["x3"] = x3;
    FMSbar_json["x4"] = x4;
    FMSbar_json["xp"] = xpade;

    Json::StyledWriter writer;
    const string output = writer.write(FMSbar_json);

    std::cout << output << std::endl;

    string filename = "./data/FMSbar_at_g_squared_eq_2" + string_const + ".txt";

    ofstream my_file;
    my_file.open (filename);
    my_file << output;
    my_file.close();

    return 0;
}