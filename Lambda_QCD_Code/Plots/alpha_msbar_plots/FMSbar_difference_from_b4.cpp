/*
Creating the arrays to be used to plot alpa_strong msbar
versus mu over Lambda msbar
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <jsoncpp/dist/json/json.h>
#include <cmath>
#include <fstream>
#include <gsl/gsl_integration.h>
#include "../../headers/Lambda_over_mu.h"
using namespace std;


int main(){
    MyLambdaMSBarOverMuClass C;

    // declare vars
    double ZETA3 = riemann_zeta(3.0);
    double ZETA4 = riemann_zeta(4.0);
    double ZETA5 = riemann_zeta(5.0);
    // As defined in paper
    double nf = 0.0;
    std::cout << "nf = " << nf << std::endl;
    double b0, b1, b2msbar, b3msbar, b4msbar;

    int array_size = 1000;
    int lower_lim = 100;
    // x = mu/Lambda
    double x0[array_size], x1[array_size], x2[array_size], x3[array_size], x4[array_size], xpade[array_size];
    double alpha[array_size];

    for (int i=lower_lim; i<array_size; ++i){
            alpha[i] = i*0.0005;
    }

    // strings used for filenames
    string string_const = "";
    if (nf != 0.0)
    {
        string_const = "_nf_eq_2";
    }

    // vars as defined in paper
    b0 = C.Getb0(nf);
    b1 = C.Getb1(nf);
    b2msbar = C.Getb2msbar(nf);
    b3msbar = C.Getb3msbar(nf, ZETA3);
    b4msbar = C.Getb4msbar(nf, ZETA3, ZETA4, ZETA5);
    
    // allocate the param structure
    my_f_params myp;
    myp.b0 = b0;
    myp.b1 = b1;
    myp.b2 = b2msbar;
    myp.b3 = b3msbar;
    myp.b4 = 0.0;

    // point to myp from inside the class
    C.setPars( &myp );


    Json::Value FMSbar_json;
    Json::Value alpha_json;
    Json::Value X0_json, X1_json, X2_json, X3_json, X4_json, XP_json;

    for (int i=lower_lim; i<array_size; ++i){
            alpha_json[i-lower_lim] = alpha[i];
            X0_json[i-lower_lim] = C.GetSolution_Analytic_b1(b0, 0.0, 0.0, alpha[i]);
            X1_json[i-lower_lim] = C.GetSolution_Analytic_b1(b0, b1, 0.0, alpha[i]);
            X2_json[i-lower_lim] = C.GetSolution_Analytic_b2(b0, b1, b2msbar, alpha[i]);
            X3_json[i-lower_lim] = C.GetSolution_Numerical(b0, b1, alpha[i]);
            myp.b4 = b4msbar;
            X4_json[i-lower_lim] = C.GetSolution_Numerical(b0, b1, alpha[i]);
            myp.b4 = 0.0;
            XP_json[i-lower_lim] = C.GetSolution_Pade_Approx(b0, b1, b2msbar, alpha[i]);
    }
    
    FMSbar_json["alpha"] = alpha_json;
    FMSbar_json["x0"] = X0_json;
    FMSbar_json["x1"] = X1_json;
    FMSbar_json["x2"] = X2_json;
    FMSbar_json["x3"] = X3_json;
    FMSbar_json["x4"] = X4_json;
    FMSbar_json["xp"] = XP_json;

    Json::StyledWriter writer;
    const string output = writer.write(FMSbar_json);

    string filename = "./data/FMSbar_difference_from_b4" + string_const + ".txt";

    ofstream my_file;
    my_file.open (filename);
    my_file << output;
    my_file.close();

    return 0;
}