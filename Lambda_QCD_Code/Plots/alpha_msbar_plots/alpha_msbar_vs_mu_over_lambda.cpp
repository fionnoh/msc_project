/*
Creating the arrays to be used to plot alpa_strong msbar
versus mu over Lambda msbar
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>
#include <fstream>
#include <gsl/gsl_integration.h>
#include "../../headers/Lambda_over_mu.h"
using namespace std;

// main routine
// ============

int main(int argc, char* argv[]){
    MyLambdaMSBarOverMuClass C;

    // declare vars
    double ZETA3 = riemann_zeta(3.0);
    double ZETA4 = riemann_zeta(4.0);
    double ZETA5 = riemann_zeta(5.0);
    // As defined in paper
    double nf = 0.0;
    std::cout << "nf = " << nf << std::endl;
    double b0, b1, b2msbar, b3msbar, b4msbar;

    int array_size = 1000;
    int lower_lim = 85;
    // x = mu/Lambda
    double x[array_size];
    double alpha[array_size];

    // strings used for filenames
    string string_const_1, string_const_2 = "";
    string_const_1 = "b0";
    if (nf != 0.0)
    {
        string_const_2 = "_nf_eq_2";
    }

    // vars as defined in paper
    b0 = C.Getb0(nf);
    b1 = 0.0; 
    b2msbar = 0.0;
    b3msbar = 0.0;
    b4msbar = 0.0;

    if (argc > 1)
    {
        b1 = C.Getb1(nf);
        string_const_1 = "b1";
    }
    if (argc > 2)
    {
        b2msbar = C.Getb2msbar(nf);
        string_const_1 = "b2";
    }
    if (argc > 3)
    {
        b3msbar = C.Getb3msbar(nf, ZETA3);
        string_const_1 = "b3";
    }
    if (argc > 4)
    {
        b4msbar = C.Getb4msbar(nf, ZETA3, ZETA4, ZETA5);
    }

    // allocate the param structure
    my_f_params myp;
    myp.b0=b0;
    myp.b1=b1;
    myp.b2=b2msbar;
    myp.b3=b3msbar;
    myp.b4=b4msbar;

    cout << "b0 = " << myp.b0 << endl;
    cout << "b1 = " << myp.b1 << endl;
    cout << "b2 = " << myp.b2 << endl;
    cout << "b3 = " << myp.b3 << endl;
    cout << "b4 = " << myp.b4 << endl;

    // point to myp from inside the class
    C.setPars( &myp );

    if (myp.b2 == 0.0) {
        std::cout << "Computing result analytically." << std::endl;
        for (int i=lower_lim; i<array_size; ++i){
            alpha[i] = i*0.0005;
            x[i] = 1.0/C.GetSolution_Analytic_b1(b0, b1, b2msbar, alpha[i]);
        }

    } else if (myp.b3 == 0.0) { // b2 !=0
        std::cout << "Computing result analytically." << std::endl;
        for (int i=lower_lim; i<array_size; ++i){
            alpha[i] = i*0.0005;
            x[i] = 1.0/C.GetSolution_Analytic_b2(b0, b1, b2msbar, alpha[i]);
        }

    } else {
        std::cout << "Computing result Numerically." << std::endl;
        for (int i=lower_lim; i<array_size; ++i){
            alpha[i] = i*0.0005;
            x[i] = 1.0/C.GetSolution_Numerical(b0, b1, alpha[i]);
        }
    }

    string filename_alpha = "./data/my_alpha_" + string_const_1 + string_const_2 + ".txt";
    string filename_x = "./data/my_x_for_alpha_" + string_const_1 + string_const_2 + ".txt";

    ofstream myfile;
    myfile.open (filename_alpha);
    for (int i=lower_lim; i<array_size; ++i){
        if ( (x[i] > 4 ) && (x[i] < 1000)){
            myfile << alpha[i] << " ";
        }
    }
    myfile.close();

    ofstream my_x_file;
    my_x_file.open (filename_x);
    for (int i=lower_lim; i<array_size; ++i){
        if ( (x[i] > 4 ) && (x[i] < 1000)){
            my_x_file << x[i] << " ";
        }
    }
    my_x_file.close();

    return 0;
}