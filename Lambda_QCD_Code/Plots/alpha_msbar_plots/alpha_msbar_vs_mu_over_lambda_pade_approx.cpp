/*
Creating the arrays to be used to plot alpa_strong msbar
versus mu over Lambda msbar
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>
#include <fstream>
#include <gsl/gsl_integration.h>
#include "../../headers/Lambda_over_mu.h"
using namespace std;

// main routine
// ============

int main(int argc, char* argv[]){

    MyLambdaMSBarOverMuClass C;

    // As defined in paper
    double nf = 0.0;
    std::cout << "nf = " << nf << std::endl;
    double b0, b1, b2msbar;

    int array_size = 1000;
    int lower_lim = 85;
    // x = mu/Lambda
    double x[array_size];
    double alpha[array_size];

    // strings used for filenames
    string string_const = "";
    if (nf != 0.0)
    {
        string_const = "_nf_eq_2";
    }

    // vars as defined in paper
    b0 = C.Getb0(nf);
    b1 = C.Getb1(nf);
    b2msbar = C.Getb2msbar(nf);

    // allocate the param structure
    my_f_params myp;
    myp.b0=b0;
    myp.b1=b1;
    myp.b2=b2msbar;

    // point to myp from inside the class
    C.setPars( &myp );

    for (int i=lower_lim; i<array_size; ++i){
            alpha[i] = i*0.0005;
            x[i] = 1.0/C.GetSolution_Pade_Approx(b0, b1, b2msbar, alpha[i]);
    }

    string filename_alpha = "./data/my_alpha_pade_approx" + string_const + ".txt";
    string filename_x = "./data/my_x_for_alpha_pade_approx" + string_const + ".txt";

    ofstream myfile;
    myfile.open (filename_alpha);
    for (int i=lower_lim; i<array_size; ++i){
        if ( (x[i] > 4 ) && (x[i] < 1000)){
            myfile << alpha[i] << " ";
        }
    }
    myfile.close();

    ofstream my_x_file;
    my_x_file.open (filename_x);
    for (int i=lower_lim; i<array_size; ++i){
        if ( (x[i] > 4 ) && (x[i] < 1000)){
            my_x_file << x[i] << " ";
        }
    }
    my_x_file.close();

    return 0;
}