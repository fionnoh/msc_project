\chapter{Lattice Methods}
\label{latt_meth}

The coupling constant on the lattice is given as $g_0(a)$, which corresponds to the $\beta$ function with coefficients $b_i^{\lat}$ and parameter $\Lambda^{\lat}$. For

\begin{equation}
	\label{F_lat}
	a\Lambda^{\lat} = F^{\lat}\left(g_0(a)\right)
\end{equation}
to be evaluated the $b_i^{\lat}$s must be known. They can be found by expanding $g_{\msbar}$ as a power series in $g_0$:

\begin{equation}
\begin{split}
   \frac{1}{g_{\msbar}^2(\mu)}
     &= \frac{1}{g_0^2(a)} + 2b_0 \ln a\mu - t^{\lat}_1
          + (2b_1 \ln a\mu - t^{\lat}_2) g_0^2(a)  \\
     &+ [-2b_0b_1 \ln^2 a\mu + 2(b_2^{\msbar} + b_1t_1^{\lat})
     \ln a\mu 
          - t_3^{\lat} ]\, g^4_0(a)
          + \ldots \,.
\end{split}
\label{gms_glat}  
\end{equation}

where it is known that \cite{arX:Bode,arX:Booth01}

\begin{equation}
\begin{split}
   t_1^{\lat} &= 0.4682013 - n_f [0.0066960 - 0.0050467\,c_{sw}  +
   0.0298435\,c_{sw}^2\\ 
   &+ am_q ( -0.0272837 + 0.0223503\,c_{sw}  - 0.0070667\,c_{sw}^2 ) +
   O((am_q))^2] \,, 
   \\[0.5em]
   t_2^{\lat} &=  0.0556675 - n_f [ 0.002600+0.000155\,c_{sw} -
   0.012834\,c_{sw}^2\\ 
       &- 0.000474\,c_{sw}^3 - 0.000104\,c_{sw}^4  + O(am_q)]  \,. 
\end{split}
\label{known_ti}
\end{equation}

and $t_3^{\lat}$ is unknown. Comparing equation (\ref{F_msbar}) to equation (\ref{F_lat}) it can been seen that

\begin{equation}
   t_1^{\lat} = 2 b_0 \ln \frac{\Lambda^{\msbar}}{\Lambda^{\lat}} \,,
\end{equation}
and
\begin{equation}
\begin{split}
   b_2^{\lat} &= b_2^{\msbar} + b_1t^{\lat}_1 - b_0t^{\lat}_2     \,,
                                         \\
   b_3^{\lat} &= b_3^{\msbar} + 2b_2^{\msbar}t_1^{\lat} 
                   + b_1 (t_1^{\lat})^2 - 2b_0 t^{\lat}_3          \,.
\end{split}
\label{bi_lat}
\end{equation}

The transformation between the two schemes is given by the $t_i^{\lat}$ and the scale running occurs through the $\ln a\mu$ terms. As $t_1^{\lat}$ is known the relationship between the $\Lambda$ parameters can be found, and as $t_2^{\lat}$ is also known the 3-loop $\beta$ function coefficient can be found. The 4-loop $\beta$ function coefficient can not be found as $t_3^{\lat}$ is not known. \\

$t_1^{\lat}$ includes terms of the bare quark mass, $m_q$, while $t_2^{\lat}$ is known only for $am_q=0$. It is possible to rewrite equation (\ref{gms_glat}) and the $t_i^{\lat}$s in such a way that the $t_i^{\lat}$s are independent of mass and $g_0^2$ is replaced by $\tilde{g}_0^2$ that contains the mass dependence. However there is little difference between extrapolating to the chiral limit using constant $\beta = 6/g_0^2$ or using constant $\tilde{\beta} = 1/\tilde{g}_0^2$. So instead of using the $t_i^{\lat}$s in (\ref{known_ti}) at finite $am_q$ the plaquette and $r_0/a$ data are first extrapolated to the chiral limit before determining $\Lambda^{\msbar}$. \\

A ``boosted'' coupling constant, $g_{\plaq}^2$, is introduced to improve the convergence of equations (\ref{gms_glat}) and (\ref{def_beta_fn}) for $\beta^{\lat}(g_0^2)$, as it is known that lattice perturbative expansions are poorly convergent. $g_{\plaq}^2$ is defined as

\begin{equation}
	\label{g_plaq_defn}
	g_{\plaq}^2 \equiv \frac{g_0^2(a)}{u_0^4}
\end{equation}

where $P \equiv u_0^4 = \langle \mbox{Tr}U^{\plaq}\rangle / 3$ is the average
plaquette. $1/g_{\plaq}^2$ is known in perturbation theory to be \cite{arX:Bali14, arX:Athenodorou}

\begin{equation}
   \frac{1}{g_{\plaq}^2} = \frac{1}{g_0^2} - p_1 - p_2g_0^2 + O(g_0^4) 
\end{equation}
where
\begin{equation}
\begin{split}
   p_1 &= \frac{1}{3}  \,,  \\
   p_2 &= 0.0339110 - n_f (0.001846 - 0.0000539\, c_{sw} + 0.001590\,c_{sw}^2) 
\end{split} \label{check}
\end{equation}
for massless clover fermions. The convergence of the series can be improve further by re-expressing the series in terms of the tadpole improved coefficient

\begin{equation}
   c_{sw}^{\plaq} = c_{sw} u_0^3 \,.
\label{csw_plaq}
\end{equation}

Rewriting $t_i^{\lat}(c_{sw})$ as $t_i^{\plaq}(c_{sw}^{\plaq})$ gives

\begin{equation}
\begin{split}
  t_1^{\plaq} 
       &= 0.1348680 - n_f [0.0066960 - 0.0050467\,c^{\plaq}_{sw}
                              + 0.0298435\,(c^{\plaq}_{sw})^2]  \\
   t_2^{\plaq} &= 0.0217565 - n_f [ 0.000753-0.001053\,c^{\plaq}_{sw}
                                  + 0.000498\,(c^{\plaq}_{sw})^2   \\
       & - 0.00047\,4(c^{\plaq}_{sw})^3 
           - 0.000104\,(c^{\plaq}_{sw})^4]  \,.
\end{split}
\label{ti_plaq}
\end{equation}

The tadpole improvement represents taking a path from $g^2 = 0$ to $g^2 = g_{\plaq}^2$ keeping $c_{sw}^{\plaq}$ fixed. If all orders of the theory were known the result would only depend on the end point, but with a finite series the trajectory does matter. Other trajectories from $0$ to $g_{\plaq}^2$ will be considered, which will help estimate systematic errors from unknown higher order terms. \\

In summary, the equations that will be used with three different methods to determine $\Lambda^{\msbar}$ are

\begin{equation}
   a \Lambda^{\plaq} = F^{\plaq}(g_{\plaq}(a)) \,, 
\label{lambda_plaq}
\end{equation}
\begin{equation}
   \frac{\Lambda^{\msbar}}{\mu} = F^{\msbar}(g_{\msbar}(\mu)) \,,
\label{lambda_msbarr}
\end{equation}
together with the conversion formula
\begin{equation}
   \frac{1}{g_{\msbar}^2(\mu)}
     = \frac{1}{g_{\plaq}^2(a)} + 2b_0 \ln a\mu - t^{\plaq}_1
          + (2b_1 \ln a\mu - t^{\plaq}_2) g_{\plaq}^2(a)
          + \ldots 
\label{gmsbarglat_full}
\end{equation}
where
\begin{equation}
\label{t1_plaq_conversion}
   t_1^{\plaq} = 2 b_0 \ln \frac{\Lambda^{\msbar}}{\Lambda^{\plaq}} 
\end{equation}
and
\begin{equation}
   b_2^{\plaq} = b_2^{\msbar} + b_1t^{\plaq}_1 - b_0t^{\plaq}_2   \,.
\label{b2_plaq}
\end{equation}

\section{Method I}

The improved tadpole coefficient depends on the coupling, so for each value of $\beta$ the corresponding $c_{sw}^{\plaq}$ term is used to compute the $t_i^{\plaq}$s from equation (\ref{ti_plaq}). Then equation (\ref{gmsbarglat_full}) is used to convert $g_{\plaq}$ to $g_{\msbar}$ for some appropriate scale $\mu_*$, and using a reference scale, say $r_0$, $r_0\Lambda^{\msbar}$ is calculated from equation (\ref{lambda_msbarr}) as

\begin{equation}
   r_0 \Lambda^{\msbar} = r_0 \mu_* F^{\msbar}( g_{\msbar}(\mu_*))  \,,
\label{r0lambda}
\end{equation}

using a numerical solution for the integral in $F^{\msbar}( g_{\msbar}(\mu_*))$. The results for each value of $\beta$ are then used to extrapolate to the continuum limit, $a \rightarrow 0$. \\

A good choice of the scale $\mu_*$ is

\begin{equation}
\label{mu_star}
   \mu_* = \frac{1}{a} \exp \left( \frac{t_1^{\plaq}}{2b_0} \right)  \,.
\end{equation}

This gives

\begin{equation}
   \frac{1}{g_{\msbar}^2(\mu_*)}
     = \frac{1}{g_{\plaq}^2(a)}
          + \left( \frac{b_1}{b_0} t^{\plaq}_1 - t^{\plaq}_2 \right)
              g_{\plaq}^2(a) + O(g_{\plaq}^4) 
\label{gmsgplaq_*}
\end{equation}

which helps the series converge as the $O(1)$ coefficients have vanished.

\section{Method II}
The values of $t_i^{\plaq}(c_{sw}^{\plaq})$ found for each value of $\beta$ can also be used to find values for $b_2^{\plaq}$ from equation (\ref{b2_plaq}) which can then be used to determine $r_0\Lambda^{\plaq}$ with equation (\ref{lambda_plaq}), giving either the analytic 3-loop solution or the Pad\'e approximation solution. $r_0\Lambda^{\plaq}$ can then be converted to $r_0\Lambda^{\msbar}$ using

\begin{equation}
   r_0\Lambda^{\msbar} = r_0\Lambda^{\plaq} 
                           \exp\left(\frac{ t_1^{\plaq}}{2 b_0} \right) \,,
\label{lam_plaq_to_lam_msbar}
\end{equation}

and again the continuum limit is taken. \\

This method is equivalent to choosing a scale $\mu_=$ such that $g_{\msbar}(\mu_=) = g_{\plaq}(a)$, and applying Method I. The scale that achieves this is:
\begin{equation}
   \mu_= = \frac{1}{a} \exp \left( \frac{t_1^{\plaq}}{2b_0} \right)
               \frac{ F^{\plaq}(g_{\plaq}(a))}{F^{\msbar}(g_{\plaq}(a))} \,.
\label{mu_equal}
\end{equation}

\section{Method III}

The final, and theoretically the most sound method, involves varying $c_{sw}^{\plaq}$ along the improvement path as $g_{\plaq}^2$ increases. This will give $\beta$ function coefficients that are independent of the coupling and, in fact, are entirely constant. The 1-loop expansion for $c_{sw}^{\plaq}$ is known along the path \cite{arX:Luscher96}

\begin{equation}
   c^{\plaq}_{sw} = 1 + c_0^{\plaq} g_{\plaq}^2 + \ldots \,, 
\label{csw_plaq_path}
\end{equation}
where $c^{\plaq}_0 = c_0 - \frac{3}{4} p_1$ and $c_0 = 0.2659(1)$. Expanding equation (\ref{ti_plaq}) gives
\begin{equation}
   b_2^{\plaq} = b_2^{\msbar} + b_1 t_1^{\plaq}\big|_{c_{sw}^{\plaq}=1} - b_0
   t_2^{\plaq}\big|_{c_{sw}^{\plaq}=1} 
                 - b_0 c_0^{\plaq} \left. 
                   \frac{\partial t_1^{\plaq}}{\partial c_{sw}^{\plaq}}
                      \right|_{c_{sw}^{\plaq}=1} 
               = -0.0008241 \,.
\label{b2_plaq_M3}
\end{equation} 

As $b_2^{\plaq}$ has been determined the same steps as in method II may now be followed, finding $r_0\Lambda^{\plaq}$ using equation (\ref{lambda_plaq}) and converting to $r_0\Lambda^{\msbar}$ using equation (\ref{lam_plaq_to_lam_msbar}). Again, $r_0\Lambda^{\plaq}$ can be found for either the analytic 3-loop solution or the Pad\'e approximation solution.

\section{Extrapolation to $n_f=3$ Flavours}
\label{nf3_extrap}

Methods I, II and III are used to evaluate $\Lambda^{\msbar}$ in the quenched, $n_f=0$, and unquenched, $n_f=2$, cases. The $\Lambda^{\msbar}$ parameter is needed for 3 flavours in order to determine $\alpha^{\msbar}_s$ for 5 flavours, which can then be compared to other lattice and phenomenological results. The $n_f=3$ $\Lambda^{\msbar}$ parameter is determined by matching the static force at a scale $r_0$.

\subsection{One-loop Matching}

As an example, the matching procedure for the 1-loop calculation is now shown. At the 1-loop level the static potential between fundamental charges is given by \cite{book:Montvay}

\begin{equation}
\label{static_pot}
\begin{split}
    V(r) = - \frac{4}{3} \frac{g^2_{\msbar}(\mu)}{4 \pi r} 
       \Bigg\{ 1 + \frac{g^2_{\msbar}(\mu)}{16 \pi^2}   
       \Bigg[ &22 \left( \ln \mu r + \gamma_E + \frac{31}{66}  \right)  \\
        &- \frac{4}{3} n_f 
          \left( \ln \mu r + \gamma_E +\frac{5}{6} \right)
          \Bigg] +\cdots \Bigg\}  
\end{split}
\end{equation} 
for massless sea quarks. Differentiating this gives the force $f(r)$ at a distance $r$ as

\begin{equation}
\label{static_force}
\begin{split}
    4 \pi r^2 f(r) = \frac{4}{3} g^2_{\msbar}(\mu)
     \Bigg\{ 1 + \frac{g^2_{\msbar}(\mu)}{16 \pi^2} \Bigg[ &22 
        \left( \ln \mu r +  \gamma_E -\; \frac{35}{66} \right)    \\
      &-  \frac{4}{3} n_f
         \left( \ln \mu r + \gamma_E -\;\frac{1}{6} \right)
         \Bigg] +\cdots \Bigg\} \,. 
\end{split}
\end{equation}

If the flavour number is changed from 2 to 0, or from 2 to 3, while keeping the force at distance $r$ constant then the expressions

\begin{equation} 
\begin{split}
 -\frac{33}{2} \ln \frac{\Lambda_0^{\msbar}}{\Lambda_2^{\msbar}} &=
  2 \left(\ln \Lambda_2^{\msbar} r + \gamma_E - \frac{1}{6} \right) \,,  \\
 (33 -6) \ln \frac{\Lambda_3^{\msbar}}{\Lambda_2^{\msbar}} &= 
   2 \left(\ln \Lambda_2^{\msbar} r + \gamma_E - \frac{1}{6} \right)  \,,
\end{split} 
\end{equation} 

can be found. Equating the expressions gives

\begin{equation} 
	\frac{\Lambda_3^{\msbar}}{\Lambda_2^{\msbar}}
	   = \left(\frac{\Lambda_2^{\msbar}}{\Lambda_0^{\msbar}}
	         \right)^{\frac{11}{18}} \,,
\end{equation} 

which gives an estimate of $\Lambda^{\msbar}_3$  from the $n_f=0$ and $n_f=2$ results.

\subsection{Higher Loops}

To perform this matching calculation with more loops a force coupling $g_{\qq}$ is defined as \cite{arX:Necco}

\begin{equation}
\label{higher_loops_force}
   4 \pi r^2 f(r) \equiv \frac{4}{3} g^2_{\qq}(r) \,.
\end{equation} 

From equation (\ref{static_force}) this gives a value for $t_1^{\qq}$ of 

\begin{equation}
\label{t1qq}
   t_1^{\qq} = -\; \frac{1}{(4 \pi)^2} 
   \left[ 22 \left( \gamma_E -\; \frac{35}{66} \right)
   -\frac{4}{3} n_f  \left( \gamma_E -\; \frac{1}{6} \right) \right] \,.
\end{equation} 

Calculating the force from the 2-loop expression of $V(r)$ from \cite{arX:Schroder} gives a value for $t_2^{\qq}$ of


\begin{equation}
\begin{split}
\label{t2qq}
 t_2^{\qq} &=  \frac{1}{(4 \pi)^4} \Bigg[ 
 \frac{1107}{2} - 204 \, \gamma_E - \; \frac{229}{3} \pi^2 
 + \frac{9}{4} \pi^4  -66\, \zeta_3 \\
 & \hspace*{2cm} 
 + \frac{n_f}{3} \left( - \; \frac{553}{3} + 76 \gamma_E +
 \frac{44}{3} \pi^2 + 52\, \zeta_3 \right) 
 + \frac{4}{27} n_f^2\, ( 12 -\pi^2 ) \Bigg] \,,
\end{split}
\end{equation} 

If the value of $f(r)$ is independent of $n_f$ at some particular distance r then by equation (\ref{higher_loops_force}) $g_{\qq}(r)$ is also independent of $n_f$. Thus in the $q\overline{q}$ scheme the $\Lambda^{\qq}$ parameters can be compared by computing 

\begin{equation}
\label{rLambda_qq}
\begin{split}
   r \Lambda^{\qq}_{0} &= F^{\qq}(g_{\qq}(r), n_f=0) \,, \\
   r \Lambda^{\qq}_{2} &= F^{\qq}(g_{\qq}(r), n_f=2) \,, \\
   r \Lambda^{\qq}_{3} &= F^{\qq}(g_{\qq}(r), n_f=3) \,. 
\end{split}
\end{equation} 


The $r \Lambda^{\qq}$ values can be converted to $r \Lambda^{\msbar}$ values by using $t_1^{\qq}$ from equation (\ref{t1qq}) with a $q\overline{q}$ scheme version of equation (\ref{t1_plaq_conversion}). Ratios of these values can be taken to cancel r and a parametric plot of $\Lambda^{\msbar}$ ratios can be made by varying $g_{\qq}$ and calculating all three $\Lambda^{\qq}$s. The plot of $\Lambda^{\msbar}_3/\Lambda^{\msbar}_2$ versus $\Lambda^{\msbar}_2/\Lambda^{\msbar}_0$ from 1, 2 and 3-loop matching is shown in Figure \ref{plot_loop_matching} in Section \ref{nf3_results}.

\section{Evaluation of $\alpha^{\msbar}_{n_f=5}(m_z)$}
\label{alpha_extra}

It is standard practice to quote values of $\alpha_s$ at a given scale, usually the mass of the Z boson. To determine $\alpha^{\msbar}_{n_f=5}(m_z)$ the value of $\Lambda^{\msbar}$ for $n_f=3$ flavours is first used in the 4-loop expansion of $\alpha_s$ \cite{arX:Chetyrkin97}


\begin{eqnarray}
\label{alp}
\frac{\alpha^{\msbar}_{n_f=3}(\mu)}{\pi}&=&\frac{1}{\beta_0L^{\msbar}_3}-\frac{b_1\ln L^{\msbar}_3}{(\beta_0L^{\msbar}_3)^2}
+\frac{1}{(\beta_0L^{\msbar}_3)^3}\left[b_1^2(\ln^2L^{\msbar}_3-\ln L^{\msbar}_3-1)+b_2\right]
\nonumber\\
&+&\frac{1}{(\beta_0L^{\msbar}_3)^4}\left[
b_1^3\left(-\ln^3L^{\msbar}_3+\frac{5}{2}\ln^2L^{\msbar}_3+2\ln L^{\msbar}_3-\frac{1}{2}\right)
\right.\nonumber\\
&-&\left.
3b_1b_2\ln L^{\msbar}_3+\frac{b_3}{2}\right],
\end{eqnarray}

where $L^{\msbar}_3 = \ln \left((\mu)^2/(\Lambda^{\msbar}_{n_f=3})^2\right)$, $b_N = \beta_N/\beta_0$, and the $\beta_i$ are coefficients of the $\beta$ function

\begin{eqnarray}
\label{rge}
\mu^2\frac{d}{d\mu^2}\,\frac{\alpha_s^{(n_f)}(\mu)}{\pi}
&=&\beta^{(n_f)}\left(\frac{\alpha_s^{(n_f)}(\mu)}{\pi}\right).
\end{eqnarray}

These are essentially the same $\beta$ function coefficients as found in Section \ref{beta_fn_theory}, but they differ by some factor if $1/4\pi$. \\

The 3-loop matching condition at the quark threshold is given for $a'= \alpha^{\msbar}_{n_l}(\mu^{(n_f)})$ and $a=\alpha^{\msbar}_{n_f}(\mu^{(n_f)})$ as \cite{arX:Chetyrkin97}

\begin{eqnarray}
\label{oms}
\frac{a'}{a}&=&1-a\frac{\mathcal{L}_h}{6}
+a^2\left(\frac{\mathcal{L}_h^2}{36}-\frac{19}{24}\mathcal{L}_h+C_2\right)
+a^3\left[-\frac{\mathcal{L}_h^3}{216}
\right.\nonumber\\
&-&\left.\vphantom{\frac{\mathcal{L}_h^3}{216}}
\frac{131}{576}\mathcal{L}_h^2+\frac{\mathcal{L}_h}{1728}(-8521+409\,n_l)+C_3
\right],
\end{eqnarray}

where $n_l = n_f - 1$, $\mathcal{L}_h = \ln[(\mu^{(n_f)})^2/M_h^2]$, $M_h$ is the pole mass, and

\begin{eqnarray}
C_2=-\frac{7}{24},\quad
C_3&=&-\frac{80507}{27648}\zeta_3
-\frac{2}{3}\zeta_2\left(\frac{1}{3}\ln2+1\right)
\nonumber\\
&-&\frac{58933}{124416}+\frac{n_l}{9}\left[\zeta_2+\frac{2479}{3456}\right].
\end{eqnarray}

This matching condition is used to extrapolate $\alpha^{\msbar}_{n_f=3}$ to $\alpha^{\msbar}_{n_f=5}$. A Mathematica package is available to perform these computations \cite{arX:Chetyrkin}.