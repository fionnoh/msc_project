
\chapter{Background Theory}
\label{back_theory}

\section{The QCD Coupling}
\label{qcd_coupling}

The QCD Lagrangian density is given by

\begin{equation}
  \mathcal{L} = \bar{\psi}_i  \left( i(\gamma^\mu D_\mu)_{ij} - m\, \delta_{ij}\right) \psi_j - \frac{1}{4}G^a_{\mu \nu} G^{\mu \nu}_a
  \label{QCD_Lag}
\end{equation}

where $\psi$ is the quark field, $m$ is the quark mass, $G^a_{\mu\nu} \equiv \partial_\mu \mathcal{A}^a_\nu - \partial_\nu \mathcal{A}^a_\mu + g f^{abc} \mathcal{A}^b_\mu \mathcal{A}^c_\nu$ is the gluon field strength with colour index a, $A^a_{\mu}$ is the gluon field, $f^{abc}$ is the SU(3) structure constant and g is the dimensionless coupling constant. 

The bare coupling, defined as $\bar{\alpha}_s = g^2/4\pi$, has no dependence on scale, however a dependence is acquired after a renormalization procedure to subtract UV divergences. Thus the ``running'' coupling for a renormalization scheme, $\schS$, is written as $\alpha^{\schS}_s(\mu) = g^2_{\schS}(\mu)/4\pi$. This running of the QCD coupling constant as the scale changes gives rise to two important properties of QCD: confinement and asymptotic freedom. At low energy scales the coupling becomes large and quarks can not be isolated in singularity. At high energy scales, i.e. processes with high momentum transfer, the coupling becomes small and quarks interact weakly. A precise knowledge of $\alpha_s$ is a key factor in the understanding of many experimental measurements at the LHC. For example the uncertainty from $\alpha_s$ is one of the dominant sources of uncertainty in the Standard Model prediction for the $H\rightarrow b\bar{b}$ partial width, and is the largest source of uncertainty for $H\rightarrow gg$ \cite{arX:Heinemeyer, arX:Dawson}. \\


\begin{figure}
\begin{center}
  \includegraphics[width=0.70\textwidth]{../figs/asq-2015.pdf}
\end{center}
   \caption{Measurements of $\alpha_s$ as a function of the energy scale Q. The respective degree of QCD perturbation theory used in the extraction of $\alpha_s$ is indicated in brackets \cite{PDG:Patrignani}.}
\label{fig_pdg_alpha}
\end{figure}

An experimental determination of $\alpha_s^{\schS}(\mu)$ is performed by measuring a short-distance quantity $\mathcal{Q}$ at a scale $\mu$ and then matching it with a perturbative expansion in terms of the running coupling:
\begin{equation}
\label{Q_expansion}
 \mathcal{Q}(\mu) = c_1\alpha_s^{\schS}(\mu) + c_2\alpha_s^{\schS}(\mu)^2 + ... 
\end{equation}
Figure \ref{fig_pdg_alpha} shows values of $\alpha_s$ that have been extracted from various experiments for different energy scales Q. For example, deep inelastic lepton and hadron scattering (DIS) data are sensitive to $\alpha_s$ at leading order through violations of Bjorken scaling by gluon bremsstrahlung from the struck quark \cite{Altarelli:1977zs}, as well as the photon-gluon fusion and pair creation processes that are described by the DGLAP (Dokshitzer-Gribov-Lipatov-Altarelli-Parisi) equations \cite{JETP:Dokshitzer}. Jet production in DIS also provides observables which can be used to determine $\alpha_s$, the production rate based on the subprocess $\gamma^*q \rightarrow qg$ is directly proportional to $\alpha_s$ is one example of this \cite{arX:Andreev}. Other examples of phenomenological results that can be used to obtain the running coupling include hadronic $\tau$ decays, hadronic final states of $e^+e^-$ annihilation, and electroweak precision fits \cite{PDG:Patrignani}. \\

However for low values of $\mu$ perturbation theory can no longer be taken advantage of, as $\alpha_s^{\schS}(\mu)$ becomes large. In order to have a full understanding of the dynamics of QCD, which may be used to reliably subtract QCD contributions from observables that probe other fundamental physics, non-perturbative methods, such as Lattice QCD, must be used. Lattice QCD discretizes the theory of QCD by discretizing spacetime on a hypercubic lattice, with distance $a$ between nearest neighbouring points and finite spacial and temporal directions. The fermion fields, $\psi(x)$, are defined on the lattice sites $x$, with $x_0 \in a\left\lbrace 0, ..., N_t-1\right\rbrace$ and $x_i \in a\left\lbrace 0, ..., N_x-1\right\rbrace$. The gauge fields, $A_{\mu}(x)$, are treated differently in order to preserve gauge invariance. The gauge potential at each lattice site is not discretized, instead the parallel transport between any site and its nearest neighbour is discretized. The continuum parallel transport

\begin{equation}
  \label{par_trans}
U_\mu(x) = \mathcal{P}exp\left(ig\int_x^{x+e_\mu}dz^\nu A_\nu(z)\right),
\end{equation}
where $\mathcal{P}$ denotes the path ordered product and $e_{\mu}$ is the vector of length $a$ in the $\mu$-direction, is used to do so. The trace of a closed loop of parallel transports is gauge invariant, the simplest of which, the plaquette, is given by

\begin{equation}
  \label{plaquette_defn}
  P_{\mu\nu}(x) = Tr\left(U_\mu(x)U_\nu(x+e_\mu)U^\dagger_\mu(x+e_\nu)U^\dagger_\nu(x) \right)
\end{equation}

and is shown in Figure \ref{fig_par_trans}. The plaquette and the discretized fermion fields can be used to create a discretized action. There are many approaches to this procedure which will results in different actions. These actions may have improved convergence in taking the limit $a\rightarrow0$ or may deal with the fermion action in different ways \cite{book:DeGrand}. \\

\begin{figure}
% \begin{center}

\setlength{\unitlength}{.04in}
\begin{picture}(75,25)(0,15)
\multiput(25,25)(10,0){9}{\circle*{2}}
\multiput(25,35)(10,0){9}{\circle*{2}}

\put(35,25){\vector(1,0){7}}\put(35,25){\line(1,0){10}}
\put(40,20){\makebox(0,0)[t]{$U_\mu(x)$}}
\put(34,28){\makebox(0,0)[r]{$x$}}

\put(75,25){\vector(-1,0){7}}\put(65,25){\line(1,0){10}}
\put(70,20){\makebox(0,0)[t]{$U^\dagger_\mu(y)$}}
\put(64,28){\makebox(0,0)[r]{$y$}}


\put(85,25){\vector(1,0){7}}\put(85,25){\line(1,0){10}}
\put(95,25){\vector(0,1){7}}\put(95,25){\line(0,1){10}}
\put(85,35){\vector(0,-1){7}}\put(85,35){\line(0,-1){10}}
\put(95,35){\vector(-1,0){7}}\put(95,35){\line(-1,0){10}}
\put(84,28){\makebox(0,0)[r]{$z$}}
\put(92,20){\makebox(0,0)[t]{$P_{\mu\nu}(z)$}}

\put(110,25){\vector(1,0){5}}
\put(117,25){\makebox(0,0)[l]{$\mu$}}
\put(110,27){\vector(0,1){5}}\put(110,34){\makebox(0,0)[b]{$\nu$}}
\end{picture}

% \end{center}
   \caption{A parallel transport, a reverse parallel transport, and an elementary plaquette.}
\label{fig_par_trans}
\end{figure}

The data used in this project come from sources that used the standard Wilson gauge action with, in the unquenched case, the non-perturbatively $O(a)$ improved clover fermion action, defined as

\begin{equation}
  \label{action}
  S = \frac{1}{3}\beta\sum_{\plaq} TrRe\left[1-P^{\plaq}_{\mu\nu}\right] + a^4\sum_{xy;q=u,d}\bar{q}(x)M^{(q)}(x;y)q(y),
\end{equation}

where $\beta = 6/g^2$ and the Wilson clover fermion matrix is given by

\begin{equation}
\begin{split}
  \label{clover_matrix}
  \sum_{xy}\bar{q}(x)&M^{(q)}(x;y)q(y) = \\
  &\sum_x\left\{\frac{1}{a}\bar{q}(x)q(x) - \frac{\kappa}{a}\sum_\mu\bar{q}(x)P^\dagger(x-ae_\mu)\left[1+\gamma_\mu\right]q(x-ae_\mu) \right.\\
  &- \frac{\kappa}{a}\sum_\mu\bar{q}(x)P(x)\left[1-\gamma_\mu\right]q(x+ae_\mu) \\
  &- \left.2\kappa ac_{sw}g_0\sum_{\mu\nu} \frac{1}{4}\bar{q}(x)\sigma_{\mu\nu}F^{clover}_{\mu\nu}(x)q(x)\right\}
\end{split}
\end{equation}

where $c_{sw}$ is the clover term coefficient, $\kappa$ is a hopping parameter related to the bare quark mass by

\begin{equation}
am_q = \frac{1}{2}\left( \frac{1}{\kappa} - \frac{1}{\kappa_c} \right) \,,
\label{amq_eqn}
\end{equation}

where $\kappa_c$ are the critical values for $\kappa$, $F^{clover}_{\mu\nu}$ is the clover field strength tensor, given by
\begin{equation}
  \label{F_clover}
  F^{clover}_{\mu\nu}(x) = \frac{1}{8ig_0a^2}\sum_{\pm\mu,\pm\nu}\left[P^{\plaq}_{\mu\nu}(x) - P^{\plaq}_{\mu\nu}(x)^{\dagger}\right],
\end{equation}
and the mass degenerate $u$ and $d$ quarks are taken. \\

In any case, as the lattice spacing is an unphysical quantity introduced to perform a calculation a physical prediction requires an extrapolation to a zero lattice spacing. As $a$ is taken to zero the calculational results are universal, regardless of the action used. \\

A lattice determination of $\alpha_s^{\schS}(\mu)$ is performed in essentially the same way as an experimental determination, the difference being the origin of the short-distance quantity, $\mathcal{Q}$. In lattice calculations $\mathcal{Q}$ is generally taken to be a combination of physical amplitudes or Euclidean correlation functions with no UV or IR divergences and a well-defined continuum limit. Examples of this include the force between static quarks or 2-point functions of quark bilinear currents. \\

Before computing $\mathcal{Q}(\mu)$ on the lattice the scale $\mu$ must first be set by using an experimentally measurable low-energy scale as an input. In this project the scales $r_0$ and $w_{0.4}$ are used. $r_0$ is defined through the force, $F(r)$, between static quarks at an intermediate distance by \cite{arX:Sommer}

\begin{equation}
  \label{r0_defn}
  r^2F(r)|_{r=R(c)} = c,
\end{equation}
with $r_0 = R(1.65)$. $c=1.65$ is chosen as this gives a value for $r_0$ of roughly $0.5$fm, varying depending on which model for $F(r)$ is used, and this is a distance at which the force is well understood. \\

$w_{0.4}$ is defined through a relation between the flow time t and the composite operator \cite{arX:Borsanyi}

\begin{equation}
   \label{comp_op}
   E(t) = \frac{1}{4}G^a_{\mu\nu}(t)G^a_{\mu\nu}(t)
 \end{equation} 

 by

 \begin{equation}
   \label{w_04_defn}
   t\frac{d}{dt}t^2\left<E(t)\right>|_{t=w^2_X}=X.
 \end{equation}

A similar reference scale can be defined with $t=t_X$, but it has been found that the discretization error of $w_X$ is suppressed more that that of $t_X$ in full QCD \cite{arX:Borsanyi}. $t\frac{d}{dt}t^2\left<E(t)\right>$ shows approximately linear behaviour within the range $0.2\leq X \leq 0.4$, and the fit of $w_{0.4}$, compared to $w_{0.2}$ or $w_{0.3}$, when introducing a parameterization of the ratio $w_{0.4}/a$ in terms of $\beta$ showed the most reasonable results \cite{arX:Asakawa}. It is for these reasons that $w_{0.4}$ was chosen as the scale. \\

After the scale is set $\mathcal{Q}$ is computed on the lattice for several lattice spacing, giving several values for $\mathcal{Q}_{\lat}(a,\mu)$. The continuum limit is then taken to find

\begin{equation}
  \label{Q_continuum}
  \mathcal{Q}(\mu) = \lim_{a\to0} \mathcal{Q}_{\lat}(a,\mu), \qquad \text{with $\mu$ fixed.}
\end{equation}

One lattice method that can be used to find $\alpha_s$ is a step-scaling method with Shr\"odinger functional boundary conditions \cite{arX:Luscher}. The determination of the running coupling is split into two calculations, one at large $\mu$ and one of a hadronic scale. The ``Schr\"odinger Functional (SF) scheme'' is a finite volume scheme in which the scale is set by the inverse lattice size, $\mu = 1/L$. For large $\mu$ the running coupling is determined while keeping the lattice spacings sufficiently small, such that

\begin{equation}
  \label{schr_fnal_scale}
  \mu = 1/L \sim 10 \ldots 100~\text{GeV}, \qquad a/L \ll 1.
\end{equation}

For calculations at a hadronic scale a certain $\alpha_s^{\text{max}} = \alpha_s\left(1/L_{\text{max}}\right)$ is chosen. Then $L_{\text{max}}/a$ and a hadronic scale such as $r_0/a$ are found for different lattice spacings. Thus a value for $L_{\text{max}}/r_0$ can be found for each $a$ and an extrapolation to the continuum can be carried out. To connect $\alpha_s\left(1/L_{\text{max}}\right)$ to $\alpha_s\left(\mu\right)$ at large $\mu$ the change of the coupling in the continuum limit is found when the lattice size is changed from $L$ to $L/2$, starting at $L=L_{\text{max}}$ and ending when $\mu = 2^k/L_{\text{max}}$. This is called step-scaling. Combining these results gives $\alpha_s(\mu)$ at $\mu = 2^k\frac{r_0}{L_{\text{max}}}\frac{1}{r_0}$. \\

Other lattice methods include determining $\alpha_s(\mu)$ from the potential between an infinitely massive quark and antiquark pair \cite{arX:Booth}, from vacuum polarizations at short distances \cite{arX:Shintani}, from current two-point functions \cite{arX:McNeile}, and from QCD vertices \cite{arX:Alles}. The method used in this project uses a 2-loop expansion of $1/g_{\msbar}(\mu)$ in terms of the lattice coupling, $g_0^2(a).$ This is described further in Chapter \ref{latt_meth}, after a discussion on the $\beta$ function.


\section{The $\beta$ Function}
\label{beta_fn_theory}

The QCD running coupling constant is a function of scale which is described by the $\beta$ function, for a scheme $\schS$, as

\begin{equation}
	\label{beta_fn}
   M\frac{\partial g_{\schS}(M)}{\partial M } = \frac{\partial g_{\schS}(M)}{ \partial \log M }
                           = \beta^{\schS}\left( g_{\schS}(M) \right)
\end{equation}

where

\begin{equation}
   \beta^{\schS} \left(g_{\schS}\right)
       = - b_0g_{\schS}^3 - b_1g_{\schS}^5
         - b_2^{\schS}g_{\schS}^7 
         - b_3^{\schS}g_{\schS}^9 - \ldots \,.
\label{def_beta_fn}
\end{equation}

The first two coefficients are scheme independent and are given as

\begin{equation}
   b_0 = \frac{1}{(4\pi)^2}
           \left( 11 - \frac{2}{3}n_f \right) \,, \qquad
   b_1 = \frac{1}{(4\pi)^4}
           \left( 102 - \frac{38}{3} n_f \right) \,.
\label{b0_b1}
\end{equation}
for the SU(3) colour gauge group. Integrating \ref{beta_fn} gives

\begin{equation}
   \frac{\Lambda^{\schS}}{M} = F^{\schS}(g_{\schS}(M)) \,,
   \label{Lamda_over_M}
\end{equation}
where
\begin{equation}
   F^{\schS}(g_{\schS})
     = \exp{\left( - \frac{1}{2b_0 g_{\schS}^2}\right)} 
         \left(b_0 g_{\schS}^2 \right)^{- \frac{b_1}{2b_0^2}}
         \exp{\left\{ - \int_0^{g_{\schS}} \! d\xi
         \left[ \frac{1}{\beta^{\schS}(\xi)} +
                \frac{1}{b_0 \xi^3} - \frac{b_1}{b_0^2\xi} \right]\right\} } \,,
\label{F_integral}
\end{equation}

and $\Lambda^{\schS}$, the integration constant, is the scheme dependent QCD scale parameter. The integral in equation (\ref{F_integral}) may be solved analytically at low orders. For example, for 3 loops, when $b_1^2 - 4b_0b_2^{\schS} \geq 0$, a solution is given by

\begin{equation}
   \frac{\Lambda^{\schS}}{M}
     = \exp{\left( - \frac{1}{2b_0 g_{\schS}^2}\right)} 
         \left(b_0 g_{\schS}^2 \right)^{- \frac{b_1}{2b_0^2}}
   \left( 1 + \frac{A^{\schS}}{2b_0} g_{\schS}^2 \right)^{-p^{\schS}_A}
   \left( 1 + \frac{B^{\schS}}{2b_0} g_{\schS}^2 \right)^{-p^{\schS}_B} \,,
\label{three_loop_exact_real}
\end{equation}
where
\begin{equation}
\begin{split}
   A^{\schS} &= b_1 + \sqrt{b_1^2 - 4b_0b_2^{\schS}} \,, \\
   B^{\schS} &= b_1 - \sqrt{b_1^2 - 4b_0b_2^{\schS}} \,,
\end{split}
\end{equation}
and
\begin{equation}
\begin{split}
   p^{\schS}_A
       &= - \frac{b_1}{4b_0^2} -  \frac{b_1^2 - 2b_0b_2^{\schS}}{4b_0^2 
                                     \sqrt{b_1^2 - 4b_0b_2^{\schS}} } \,,
                                          \\
   p^{\schS}_B
       &= - \frac{b_1}{4b_0^2} +  \frac{b_1^2 - 2b_0b_2^{\schS}}{4b_0^2 
                                     \sqrt{b_1^2 - 4b_0b_2^{\schS}} } \,.
\end{split}
\end{equation}

When $b_1^2 - 4b_0b_2^{\schS} < 0$ a solution is given by

\begin{equation}
   \frac{\Lambda^{\schS}}{M}
     = \exp{\left( - \frac{1}{2b_0 g_{\schS}^2}\right)} 
         \left(b_0 g_{\schS}^2 \right)^{- \frac{b_1}{2b_0^2}}
   \left( \xi^2 + \lambda^2 \right)^{\eta}
   \exp\left( -2\tan^{-1}\left( \frac{\lambda}{\xi}\right)\kappa \right) \,,
\label{three_loop_exact_im}
\end{equation}

where
\begin{equation}
	\xi = 1 + \frac{b_1}{2b_0}g_{\schS}^2
\end{equation}
\begin{equation}
	\lambda = \frac{\sqrt{4b_0b_2^{\schS} - b_1^2}}{2b_0}g_{\schS}^2
\end{equation}

\begin{equation}
	\eta = \frac{1}{16b_0^4}\left(b_1^2 - \frac{b_1^4 - 4b_0b_1^2b_2^{\schS} + 4b_0^2\left(b_2^{\schS}\right)^2}{4b_0b_2^{\schS} - b_1^2} \right)
\end{equation}

\begin{equation}
	\kappa = \frac{1}{16b_0^4}\left( \frac{2b_1^3 - 4b_0b_1b_2^{\schS}}{\sqrt{4b_0b_2^{\schS} - b_1^2}}\right)
\end{equation}

Results are usually given in the $\overline{MS}$ scheme, with scale $\mu$
\begin{equation}
	\frac{\Lambda^{\msbar}}{\mu} = F^{\msbar}\left( g_{\msbar}(\mu)\right).
	\label{F_msbar}
\end{equation}

In this scheme the next three $\beta$ function coefficients are known \cite{arX:Baikov, arX:Herzog}, 

\begin{equation}
\begin{split}
   b_2^{\msbar}
        &= \frac{1}{(4\pi)^6}
            \left( \frac{2857}{2} - \frac{5033}{18}n_f 
                    + \frac{325}{54}n_f^2 \right)  \,,  \\
   b_3^{\msbar}
       &= \frac{1}{(4\pi)^8}
           \left[ \frac{149753}{6} + 3564\,\zeta_3 \right.
         - \left(\frac{1078361}{162} + \frac{6508}{27}\zeta_3\right)n_f  \\
       &   \left. \hspace*{3.23cm}
            + \left(\frac{50065}{162}+ {6472}{81}\zeta_3\right)n_f^2
            + \frac{1093}{729}n_f^3 \right] \,, \\
   b_4^{\msbar}
    	&= \frac{1}{(4\pi)^{10}}
           \left[ \frac{8157455}{16} + \frac{621885}{2}\,\zeta_3 - \frac{88209}{2}\,\zeta_4 - 288090\,\zeta_5 \right. \\
        & \hspace*{0.5cm} + \left(-\frac{336460813}{1944} - \frac{4811164}{81}\zeta_3 + \frac{33935}{6}\zeta_4 + \frac{1358995}{27}\zeta_5\right)n_f  \\
        & \hspace*{0.5cm} + \left(\frac{25960913}{1944} + {698531}{81}\zeta_3 - {10526}{9}\zeta_4 - {381760}{81}\zeta_5 \right)n_f^2 \\
        & \hspace*{0.5cm} + \left(-\frac{630559}{5832} - \frac{48722}{243}\zeta_3 + \frac{1618}{27}\zeta_4 + \frac{460}{9}\zeta_5\right)n_f^3 \\
        & \left. \hspace*{5.0cm} + \left(\frac{1205}{2916} - \frac{152}{81}\zeta_3\right)n_f^4 \right] \,.
\end{split}
\label{beta_msbar}
\end{equation}

In many schemes the $\beta$ function is only known to three loops. To improve convergence a Pad\'e approximation of the $\beta$ function is written as

\begin{equation}
   \beta^{\schS}_{[1/1]}(g_{\schS}) = 
      - \frac{ b_0g_{\schS}^3 + 
          \left( b_1 - \frac{b_0b_2^{\schS}}{b_1} \right) g_{\schS}^5}
          {1 - \frac{b_2^{\schS}}{b_1} g_{\schS}^2 } \,.
\label{beta_11pade}
\end{equation}

This approximation can be expanded and arranged to give the first three coefficients of (\ref{beta_fn}) and the next coefficient, $b_3^{\schS}$, is estimated as 

\begin{equation}
   b_3^{\schS} \approx \frac{ (b_2^{\schS})^2}{b_1} \,.
\label{b3approx}
\end{equation}

An analytic solution for $F^{\schS}$ can be found using $\beta^{\schS}_{[1/1]}(g_{\schS})$ to be

\begin{equation}
   \frac{\Lambda^{\schS}}{M}
     = \exp{\left( - \frac{1}{2b_0 g_{\schS}^2}\right)}
         \left[ \frac{b_0 g_{\schS}^2}{1 + \left( \frac{b_1}{b_0} - 
                                                  \frac{b_2^{\schS}}{b_1}
                                           \right) g_{\schS}^2}
   \right]^{-\frac{ b_1}{2b_0^2}} \,.
\label{beta_11pade_exact}
\end{equation}

The running coupling $\alpha^{\msbar}_s(\mu) \equiv g^2_{\msbar}(\mu)/4\pi$ is plotted versus $\mu/\Lambda^{\msbar}$ in Figure \ref{fig_alpha_msbar} for $n_f = 0, 2.$ These values were found by solving (\ref{F_integral}), either numerically or analytically where appropriate, using only the first coefficient, the first and second coefficients, etc. of the $\beta$ function, as well as the analytic result using the Pad\'e approximation. \\

\begin{figure}%
    \centering
    {{\includegraphics[width=7.3cm]{../plots/alpha_msbar_vs_mu_over_lambda_combined.pdf} }}%
    \qquad
    {{\includegraphics[width=7.3cm]{../plots/alpha_msbar_vs_mu_over_lambda_combined_nf_eq_2.pdf} }}%
    \caption{$\alpha_s^{\msbar}(\mu)$ versus
            $\mu/\Lambda^{\msbar}$ for $n_f=0$
            and $n_f=2$, using successively
            more and more coefficients of the $\beta$ function.}%
    \label{fig_alpha_msbar}%
\end{figure}

It can be seen that the series converges rapidly, it is difficult to tell the difference between the $b_2$, $b_3$ and $b_4$ results. The Pad\'e approximation can also be seen to work well. The main difference between the $n_f = 0$ and $n_f = 2$ results is that $\alpha^{\msbar}_s|_{n_f=2}$ rises more steeply as a function of $\mu/\Lambda^{\msbar}$, as $b_0|_{nf=2} < b_0|_{n_f=0}$. \\

Figure \ref{fig_deltaF} shows $\Delta F^{\msbar}$ plotted versus $\alpha_s^{\msbar}$ for $n_f = 2$, where $\Delta F^{\msbar}$ is the difference between $F^{\msbar}$ found by solving (\ref{F_integral}) numerically with the first five coefficients of the $\beta$ function and $F^{\msbar}$ found with the first coefficient, the first and second coefficients, etc. of the $\beta$ function, as well as the Pad\'e approximation. Again, it can be seen that the convergence of the series is rapid and that the Pad\'e approximation is a suitable substitute, especially in the range $\alpha_s^{\msbar} \sim 0.1$, which is the range that the final result of this project is in.


\begin{figure}
\begin{center}
  \includegraphics[width=1.0\textwidth]{../plots/FMSbar_difference_from_b4_nf_eq_2.pdf}
\end{center}
   \caption{$\Delta F^{\msbar}$ versus $\alpha_s^{\msbar}$ for $n_f=2$ where $\Delta F^{\msbar}$ is the difference between $F^{\msbar}$ found with the first five coefficients of the $\beta$ function and $F^{\msbar}$ found with successively less $\beta$ function coefficients, as well as the Pad\'e approximation result.}
\label{fig_deltaF}
\end{figure}