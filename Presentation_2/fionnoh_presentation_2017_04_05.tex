% $Header$

\documentclass{beamer}

% This file is a solution template for:

% - Talk at a conference/colloquium.
% - Talk length is about 20min.
% - Style is ornate.



% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 

\newcommand{\lat}{\mbox{\tiny $L\!A\!T$}}                % LAT
\mode<presentation>
{
  % \usetheme{Warsaw}
  \usetheme{Singapore}
  % or ...

  % \setbeamercovered{transparent}
  % or whatever (possibly just delete it)
}


\usepackage[english]{babel}
% or whatever

\usepackage[latin1]{inputenc}
% or whatever

\usepackage{times}
\usepackage[T1]{fontenc}


\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bbold}
\usepackage{bm}

% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.


\title[A Determination of the Lambda Parameter from Full Lattice
QCD.] % (optional, use only with long paper titles)
{A Determination of the Lambda Parameter from Full Lattice
QCD.}

% \subtitle
% {Include Only If Paper Has a Subtitle}

\author[M. G{\"o}ckeler, R. Horsley, A. C. Irving, D. Pleiter, P. E. L. Rakow, G. Schierholz
and H. St{\"u}ben.] % (optional, use only with lots of authors)
{M. G{\"o}ckeler, R. Horsley, A. C. Irving, D. Pleiter, P. E. L. Rakow, G. Schierholz
and H. St{\"u}ben.}
% - Give the names in the same order as the appear in the paper.
% - Use the \inst{?} command only if the authors have different
%   affiliation.

% \institute[Universities of Somewhere and Elsewhere] % (optional, but mostly needed)
% {
%   \inst{1}%
%   Department of Computer Science\\
%   University of Somewhere
%   \and
%   \inst{2}%
%   Department of Theoretical Philosophy\\
%   University of Elsewhere}
% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.

\date[2017] % (optional, should be abbreviation of conference name)
{Fionn \'O h\'Og\'ain: MSc. Presentation 2, 2017}
% - Either use conference name or its abbreviation.
% - Not really informative to the audience, more for people (including
%   yourself) who are reading the slides online

% \subject{Theoretical Computer Science}
% This is only inserted into the PDF information catalog. Can be left
% out. 



% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}



% Delete this, if you do not want the table of contents to pop up at

% \AtBeginSubsection[]
% {
%   \begin{frame}<beamer>{Outline}

%   \end{frame}
% }


% If you wish to uncover everything in a step-wise fashion, uncomment
% the following command: 

% \beamerdefaultoverlayspecification{<+->}


\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\section{Introduction}





% \begin{frame}{Outline}
%   \tableofcontents
%   % You might wish to add the option [pausesections]
% \end{frame}




\begin{frame}{Overview of the Presentation}
  % - A title should summarize the slide in an understandable fashion
  %   for anyone how does not follow everything on the slide itself.

  \begin{itemize}
  \item
    An introduction to quantum chromodynamics and the Lambda parameter.
  \item
    An introduction to lattice QCD.
  \item
    Using lattice methods to determine the Lambda parameter.
  \end{itemize}
\end{frame}


\section{QCD}

\begin{frame}{QCD}
  \begin{itemize}
  \item The theory of the strong nuclear force:
    \begin{itemize}
    \item
      Confines quarks into hadron particles
    \item    
      Binds protons and neutrons to create atomic nuclei.
    \item    
      Interaction is mediated by the gluon.
    \end{itemize}
  \end{itemize}
  \begin{center}
  \includegraphics[width=0.75\textwidth]{FIGS/neuton_and_proton.png}
  \end{center}
\end{frame}


\begin{frame}{QCD Lagrangian}

\setbeamercolor{normal text}{fg=gray,bg=}
\setbeamercolor{alerted text}{fg=black,bg=}
\usebeamercolor{normal text}
The gauge invariant QCD Lagrangian is given by:

\begin{equation*} \label{QCD_Lagrangian}
\mathcal{L}_\mathrm{QCD} = \bar{\psi}_i  \left( i(\gamma^\mu D_\mu)_{ij} - m\, \delta_{ij}\right) \psi_j - \frac{1}{4}G^a_{\mu \nu} G^{\mu \nu}_a
\end{equation*}

\begin{itemize}
\footnotesize{
\item Quark field : $\psi_i(x)$\\
\item Dirac matrices: $\gamma_\mu$ \\
\item Gauge covariant derivative: $D_\mu \equiv \partial_\mu - igA^a_\mu(x)t^a $ \\
\item Gauge invariant gluon field strength tensor: $G^a_{\mu \nu} \equiv \partial_\mu \mathcal{A}^a_\nu - \partial_\nu \mathcal{A}^a_\mu + g f^{abc} \mathcal{A}^b_\mu \mathcal{A}^c_\nu$\\
\item Gluon fields: $\mathcal{A}^a_\mu$\\
\item Structure constants of SU(3): $f^{abc}$\\
\item Quark mass: $m$\\
\item \alert<+>{Coupling constant: $g$}
}
\end{itemize}

\end{frame}



\begin{frame}{The dependence of coupling on scale}
Effective coupling becomes large at ``low'' energy scales, and small at ``high'' energy scales.

\begin{center}
  \includegraphics[width=0.65\textwidth]{FIGS/alpha_dep_on_scale.png}
  \end{center}

This property leads to two important features of QCD, asymptotic freedom and confinement.

\end{frame}


\section*{The Lambda Parameter}

\begin{frame}{Beta Function And Lambda Parameter}
The dependence of a coupling, g, on an energy scale, M, can be expressed with the Beta function:

\begin{equation*}
\frac{\partial g_S(M)}{\partial lnM} = \beta^S\left(g_S(M)\right)
\end{equation*}
with
\begin{equation*}
\beta^S(g_S) = -b_0g_S^3-b_1g_S^5-b_2^Sg_S^7-b^S_3g_S^9
\end{equation*}
where S denotes the scheme we use.\\

\end{frame}

\begin{frame}{Beta Function And Lambda Parameter}

Integrating the beta function gives:

\begin{equation*}
   \frac{~\Lambda^{S}}{M} = F^{S}(g_{S}(M))
\end{equation*}
with
\begin{multline*}
   F^{S}(g_{S}(M)) = \exp{\left( - {1\over 2b_0 g_{S}^2}\right)} 
         \left(b_0 g_{S}^2 \right)^{- {b_1\over 2b_0^2}} \\
         \exp{\left\{ - \int_0^{g_{S}} \! d\xi
         \left[ {1 \over \beta^{S}(\xi)} +
                {1\over b_0 \xi^3} - {b_1\over b_0^2\xi} \right]\right\} }
\end{multline*}
\end{frame}

\begin{frame}{Beta function coefficients}
The first two coefficients are scheme independent and are given for the SU(3) color gauge group as

\begin{equation*}
b_0 = {1\over (4\pi)^2}
           \left( 11 - {2\over 3}n_f \right) \,, \qquad
   b_1 = {1\over (4\pi)^4}
           \left( 102 - {38 \over 3} n_f \right) \,.
\end{equation*}

In the $\overline{MS}$ scheme the next two $\beta$ function coefficients are 
known:

\begin{multline*}
   b_2^{\overline{MS}}={1\over (4\pi)^6}
            \left( {2857\over 2} - {5033\over 18}n_f 
                    + {325\over 54}n_f^2 \right)    \qquad \qquad \qquad \qquad \qquad
\end{multline*}
~
\begin{multline*}
   b_3^{\overline{MS}}
       = {1\over (4\pi)^8}
           \left[ {149753\over 6} + 3564\,\zeta_3 \right.
         - \left({1078361\over 162} + {6508\over 27}\zeta_3\right)n_f  \\
            \left. + \left({50065\over 162}+ {6472\over 81}\zeta_3\right)n_f^2
            + {1093\over 729}n_f^3 \right]
\end{multline*}

\end{frame}

\begin{frame}{Choice of scale}
Need to specify what is meant by M. There are several choices:
\begin{itemize}
\item 1/L: \qquad Schr\"odinger Functional
\item 2/r: ~\qquad heavy quark-antiquark potential
\item q: ~~~\qquad observables in momentum space
\item 2$\bar{m}_h$: ~~\quad moments of heavy-quark currents
\end{itemize}
Popular choice is $r_0$, the force scale, which is related to the force between static quarks. It is common to see results in terms of $r_0\Lambda^{S}$.
\end{frame}


\section{Lattice QCD}


\begin{frame}{Lattice QCD}
Discretize the theory on a finite space-time lattice
  \begin{itemize}
    \item Lattice is hypercubic with distance $a$ between nearest neighbouring points
    \item Finite spacial and temporal directions: $L = N_xa$, \quad $T = N_ta$
    \item Fermion fields, $\psi(x)$, are defined on the lattice sites x with $x_0 \in a\left\lbrace 0, ..., N_t-1\right\rbrace$ and $x_i \in a\left\lbrace 0, ..., N_x-1\right\rbrace$
    \item Gauge fields, $A_\mu(x)$, are treated differently...
  \end{itemize}
 \begin{center}
\setlength{\unitlength}{.025in}
\begin{picture}(75,75)(0,0)
\multiput(25,25)(10,0){4}{\circle*{2}}
\multiput(25,35)(10,0){4}{\circle*{2}}
\multiput(25,45)(10,0){4}{\circle*{2}}
\multiput(25,55)(10,0){4}{\circle*{2}}

\put(60,40){\vector(0,1){15}}
\put(60,40){\vector(0,-1){15}}
\put(65,40){\makebox(0,0){$L$}}

\put(50,20){\vector(1,0){5}}
\put(50,20){\vector(-1,0){5}}
\put(50,15){\makebox(0,0)[t]{$a$}}

\put(11,55){\vector(1,0){8}}
\put(9,55){\makebox(0,0)[r]{site}}

\put(11,30){\vector(1,0){8}}
\put(9,30){\makebox(0,0)[r]{link}}
\put(25,25){\line(0,1){10}}

\end{picture}
\end{center}

\end{frame}

\begin{frame}{Discretizing gauge fields}
We discretize the parallel transport between any site and its nearest neighbour:

\begin{equation*} \label{par_trans}
U_\mu(x) = \mathcal{P}exp\left(ig\int_x^{x+e_\mu}dz^\nu A_\nu(z)\right)
\end{equation*} 

$\mathcal{P}$: path-ordering operator \\
$e_\mu$: vector of length a in the $\mu$ direction

\begin{flushleft}
\setlength{\unitlength}{.03in}
\begin{picture}(75,25)(0,15)
\multiput(25,25)(10,0){9}{\circle*{2}}
\multiput(25,35)(10,0){9}{\circle*{2}}

\put(35,25){\vector(1,0){7}}\put(35,25){\line(1,0){10}}
\put(40,20){\makebox(0,0)[t]{$U_\mu(x)$}}
\put(34,28){\makebox(0,0)[r]{$x$}}

\put(75,25){\vector(-1,0){7}}\put(65,25){\line(1,0){10}}
\put(70,20){\makebox(0,0)[t]{$U^\dagger_\mu(y)$}}
\put(64,28){\makebox(0,0)[r]{$y$}}


\put(85,25){\vector(1,0){7}}\put(85,25){\line(1,0){10}}
\put(95,25){\vector(0,1){7}}\put(95,25){\line(0,1){10}}
\put(85,35){\vector(0,-1){7}}\put(85,35){\line(0,-1){10}}
\put(95,35){\vector(-1,0){7}}\put(95,35){\line(-1,0){10}}

\put(110,25){\vector(1,0){5}}
\put(117,25){\makebox(0,0)[l]{$\mu$}}
\put(110,27){\vector(0,1){5}}\put(110,34){\makebox(0,0)[b]{$\nu$}}
\end{picture}
\end{flushleft}
\end{frame}

\section{Lattice Methods}

\begin{frame}{Lattice Methods}
On the lattice the action used in the quenched limit is

\begin{equation*}
S = \frac{1}{3}\beta\sum_{\square}Tr\left(1 - U^{\square}_{\mu\nu}\right) + a^4 \sum_{xy;q=u,d} \bar{q}(x)M^{(q)}(x;y)q(y)
\end{equation*}
where
\begin{itemize}
\footnotesize{
\item $\beta = \frac{2N}{g^2}$
\item $U^{\square}_{\mu\nu}$ is the product of links around a plaquette
\item The summation over xy is the Wilson (clover) fermion matrix
}
\end{itemize}
We also have a coupling constant $g_0(a)$ and corresponding
$\beta$ function with coefficients $b_i^{\lat}$ and parameter
$\Lambda^{\lat}$, where
\begin{equation*}
   a \Lambda^{\lat}  = F^{\lat}(g_0(a)) \,.
\label{lambda_lat}
\end{equation*}
\end{frame}


\begin{frame}{Boosted coupling}
Look at a ``boosted'' coupling constant to help with convergence of the beta function

\begin{equation*}
g^2_\square = \frac{g^2_0(a)}{u_0^4}
\end{equation*}
where

\begin{equation*}
u_0^4 = \frac{1}{3} \left< TrU^{\square} \right>
\end{equation*}
is the average plaquette. \\

\end{frame}

\begin{frame}{Converting boosted to $\overline{MS}$}
Expand $g_{\overline{MS}}$ as a power series in $g_\square$:

\begin{equation*}
\frac{1}{g_{\overline{MS}}^2(\mu)} = \frac{1}{g_\square^2(a)} + 2b_0 \ln a\mu - t^{\square}_1
          + (2b_1 \ln a\mu - t^{\square}_2) g_0^2(a) \ldots \,.
\end{equation*}
where the $t_i^\square$ are given by:
\begin{equation*}
t_1^{\square} = 0.1348680 - n_f [0.0066960 - 0.0050467\,c^{\square}_{sw}
                              + 0.0298435\,(c^{\square}_{sw})^2]
\end{equation*}
\begin{multline*}
   t_2^{\square} = 0.0217565 - n_f [ 0.000753+0.001053\,c^{\square}_{sw}
                                  - 0.000498\,(c^{\square}_{sw})^2   \\
       - 0.00047\,4(c^{\square}_{sw})^3 
           - 0.000104\,(c^{\square}_{sw})^4]  \,.
\end{multline*}
and
\begin{equation*}
c^{\square}_{sw} = c_{sw}u_0^3
\end{equation*}
\end{frame}

\begin{frame}{Consistency between schemes}
To have consistency between $F^{\overline{MS}}(g_{\overline{MS}}(\mu))$ and $F^{\square}(g_\square(a))$ we need
\begin{equation*}
t_1^{\square} = 2 b_0 \ln \frac{\Lambda^{\overline{MS}}}{\Lambda^{\square}} 
\end{equation*}
and 
\begin{equation*}
b_2^{\square} = b_2^{\overline{MS}} + b_1t^{\square}_1 - b_0t^{\square}_2
\end{equation*}
\end{frame}

\begin{frame}{Method I}
For different values of $\beta$ $t^\square_i$ are found and used to find $g_{\overline{MS}}$ from $g_\square$. Then $r_0\Lambda^{\overline{MS}}$ can be calculated for some $\mu_\ast$
\begin{equation*}
r_0\Lambda^{\overline{MS}} = r_0\mu_\ast F^{\overline{MS}}(g_{\overline{MS}}(\mu_\ast))
\end{equation*}
$\mu_\ast$ can be chosen as

\begin{equation*}
\mu_\ast = \frac{1}{a} exp\left( \frac{t_1^\square}{2b_0} \right)
\end{equation*}

to simplify the $g_{\overline{MS}}$ power series and improve its convergence. This gives

\begin{equation*}
   {1 \over g_{\overline{MS}}^2(\mu_*)}
     = {1 \over g_{\square}^2(a)}
          + \left( {b_1 \over b_0} t^{\square}_1 - t^{\square}_2 \right)
              g_{\square}^2(a) + O(g_{\square}^4) 
\end{equation*}

\end{frame}

\begin{frame}{Method II}
Use $t^\square_i$ to determine $b_2^\square$ and then find $r_0\Lambda^\square = \frac{r_0}{a}F^{\square}(g_\square(a))$. After this, convert to $r_0\Lambda^{\overline{MS}}$ by
  \begin{equation*}
    r_0\Lambda^{\overline{MS}} = r_0\Lambda^{\square} 
                           \exp{ \left( t_1^{\square} \over 2 b_0 \right) }
  \end{equation*}

This is equivalent to choosing a scale, $\mu_=$ such that $g_{\overline{MS}}(\mu_=) = g_\square(a)$, which is given by


\begin{equation}
   \mu_= = {1\over a} \exp \left( {t_1^{\square} \over 2b_0} \right)
               { F^{\square}(g_{\square}(a)) \over F^{\overline{MS}}(g_{\square}(a))} \,.
\label{mu_equal}
\end{equation}

\end{frame}

\begin{frame}{Method III}
Vary $c_{sw}^{\square}$ along a path as $g_{\square}^2$ increases. 
This will give constant $\beta$ function coefficients.
The 1-loop expansion for $c_{sw}^{\square}$ is known along this path,
\begin{equation*}
   c^{\square}_{sw} = 1 + c_0^{\square} g_{\square}^2 + \ldots \,, 
\label{csw_square}
\end{equation*}
with $c^{\square}_0 = c_0 - \frac{1}{4}$ and $c_0 = 0.2659(1)$
then expanding the $t_i^\square$ gives
\begin{equation*}
   b_2^{\square} = b_2^{\overline{MS}} + b_1 t_1^{\square}\big|_{c_{sw}^{\square}=1} - b_0
   t_2^{\square}\big|_{c_{sw}^{\square}=1} 
                 - b_0 c_0^{\square} \left. 
                   {\partial t_1^{\square} \over \partial c_{sw}^{\square}}
                      \right|_{c_{sw}^{\square}=1} 
               = -0.0008241 \,.
\end{equation*}

With this $b_2^\square$ the same method as before is used, first finding $r_0 \Lambda^\square$, and then converting to $r_0\Lambda^{\overline{MS}}$.
\end{frame}

\begin{frame}{Pad\'e Approximation}
No knowledge of $t_3^\square$ meant that there was no value for $b_3^\square$. Look at an approximation:
  \begin{equation*}
    \beta^{\cal S}_{[1/1]}(g_{\cal S}) = 
      - { b_0g_{\cal S}^3 + 
          \left( b_1 - {b_0b_2^{\cal S}\over b_1} \right) g_{\cal S}^5 \over
          1 - {b_2^{\cal S}\over b_1} g_{\cal S}^2 }
  \end{equation*}
Upon expansion this gives the first three $\beta$ function coefficients and estimates the fourth as:

\begin{equation}
   b_3^{\cal S} \approx { (b_2^{\cal S})^2 \over b_1}
\end{equation}

\end{frame}

\begin{frame}{Quenched Results}
In the quenched ($n_f = 0$) case there is no difference between method II and III.

\begin{figure}
   \includegraphics[scale=0.30]{FIGS/fig_r0lamMSbar_lbfun_nf0_IIP_8pts.eps}
   \caption{$r_0\Lambda^{\overline{MS}}$ points versus $(a/r_0)^2$, 
            for method IIP.}
\end{figure}
\begin{equation*}
 \left. r_0\Lambda^{\overline{MS}}\right|_{n_f=0} =
 0.614(2)(5), \qquad \Lambda^{\overline{MS}}_0 = 259(1)(20)\,\mbox{MeV}
\end{equation*}
\end{frame}

\begin{frame}{Comparison With Other Results}
\begin{figure}
   \includegraphics[scale=0.35]{FIGS/r0LamMSbar_160622.pdf}
   \caption{Comparison of $r_0\Lambda^{\overline{MS}}$ estimates.}
\end{figure}
\end{frame}



\section*{Summary}

\begin{frame}{Summary}

  % Keep the summary *very short*.
  \begin{itemize}
  \item
    The QCD coupling constant, g, can be related to the dimensionful parameter $\Lambda$
  \item
    Lattice QCD is the method used to determine $\Lambda$
  \item
    The $\Lambda$ parameter is found to be $259(1)(20)\,\mbox{MeV}$ in the quenched case
  \end{itemize}
  
\end{frame}

\begin{frame}[allowframebreaks]
  \frametitle<presentation>{References}
    
  \begin{thebibliography}{10}
    
  \beamertemplatebookbibitems
  % Start with overview books.

  \bibitem{MontvayMunster1994}
    I. Montvay, G. M{\"u}nster.
    \newblock {\em Quantum Fields on a Lattice}.
    \newblock Cambridge University Press, 1994.
 
    
  \beamertemplatearticlebibitems
  % Followed by interesting articles. Keep the list short. 

  \bibitem{FlagPaper}
    S. Aoki et al.
    \newblock Review of lattice results concerning low-energy particle physics
    \newblock {\em arXiv:1607.00299}



  \end{thebibliography}
\end{frame}

\section*{}
\begin{frame}{}
\begin{center}
Thank you. \\
Any questions?
\end{center} 
\end{frame}



% All of the following is optional and typically not needed. 
% \appendix
% \section<presentation>*{\appendixname}


% \begin{frame}[allowframebreaks]
%   \frametitle<presentation>{For Further Reading}
    
%   \begin{thebibliography}{10}
    
%   \beamertemplatebookbibitems
%   % Start with overview books.

%   \bibitem{Author1990}
%     A.~Author.
%     \newblock {\em Handbook of Everything}.
%     \newblock Some Press, 1990.
 
    
%   \beamertemplatearticlebibitems
%   % Followed by interesting articles. Keep the list short. 

%   \bibitem{Someone2000}
%     S.~Someone.
%     \newblock On this and that.
%     \newblock {\em Journal of This and That}, 2(1):50--100,
%     2000.
%   \end{thebibliography}
% \end{frame}

\end{document}


